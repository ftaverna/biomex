# Libraries
library(data.table)
library(clusterProfiler)

# Read human file
humanCellSurface <- fread("Human cell surface.csv")
humanCellSurface <- humanCellSurface[[2]]

# Read mouse file
mouseCellSurface <- fread("Mouse cell surface.csv")
mouseCellSurface <- mouseCellSurface[[2]]

# Read the KEGG rda object (which needs to be updated)
load("../KEGG.rda")

# Conver the human SYMBOL to something matching with the other species
cellsurfaceHumanStyle <- humanCellSurface
cellsurfaceMouseStyle <- mouseCellSurface 
cellsurfaceZebrafishStyle <- tolower(cellsurfaceHumanStyle) # The zebrafish style is like the human but all lower

# Convert to ENTREZID for all species
hs <- bitr(cellsurfaceHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Hs.eg.db")
mm <- bitr(cellsurfaceMouseStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Mm.eg.db")
rn <- bitr(cellsurfaceMouseStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Rn.eg.db")
bt <- bitr(cellsurfaceHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Bt.eg.db")
dr <- bitr(cellsurfaceZebrafishStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Dr.eg.db")
ss <- bitr(cellsurfaceHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Ss.eg.db")
mf <- bitr(cellsurfaceHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Mfascicularis.eg.db")

# Add the cellsurface to the KEGG object
KEGG$Human$CellSurface <- hs[[2]]
KEGG$Mouse$CellSurface <- mm[[2]]
KEGG$Rat$CellSurface <- rn[[2]]
KEGG$Cow$CellSurface <- bt[[2]]
KEGG$Zebrafish$CellSurface <- dr[[2]]
KEGG$Pig$CellSurface <- ss[[2]]
KEGG$`Crab-eating macaque`$CellSurface <- mf[[2]]


# Check if the feature were added
KEGG$Human$CellSurface
KEGG$Mouse$CellSurface
KEGG$Rat$CellSurface
KEGG$Cow$CellSurface
KEGG$Zebrafish$CellSurface
KEGG$Pig$CellSurface
KEGG$`Crab-eating macaque`$CellSurface

# Save the results to the KEGG object
save(KEGG, file = "KEGG.rda")
