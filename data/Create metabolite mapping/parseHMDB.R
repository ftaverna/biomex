library(XML)
library(data.table)

# To use this script, you need to download the hmdb_metabolites.xml from the HMDB website.
# Then you need to remove the xmlns tag from the <hmdb> tag (which is the second line)
# You need to use an ad-hc editor to do it, like 010 Editor for Windows.

xmlFile <- xmlParse("hmdb_metabolites.xml")
metabolites <- xpathSApply(xmlFile, "//metabolite")

values <- list()
i <- 1
for (metabolite in metabolites)
{
  print(i)
  
  hmdb <- xpathSApply(metabolite, "accession", xmlValue)
  name <- xpathSApply(metabolite, "name", xmlValue)
  kegg <- xpathSApply(metabolite, "kegg_id", xmlValue)
  metlin <- xpathSApply(metabolite, "metlin_id", xmlValue)
  secondary_accessions <- xpathSApply(metabolite, "secondary_accessions/accession", xmlValue)
  n <- length(secondary_accessions)
  
  if (length(hmdb) == 0) hmdb <- ""
  if (length(name) == 0) name <- ""
  if (length(kegg) == 0) kegg <- ""
  if (length(metlin) == 0) metlin <- ""
  if (length(secondary_accessions) == 0) secondary_accessions <- ""
  
  
  if (n >= 1)
  {
    hmdb <- c(hmdb, secondary_accessions)
    name <- rep(name, n + 1)
    kegg <- rep(kegg, n + 1)
    metlin <- rep(metlin, n + 1)
  }
  
  for (j in 1:length(hmdb))
  {
    values[[length(values) + 1]] <- c(name[j], hmdb[j], kegg[j], metlin[j])
  }
  
  i <- i + 1
}

mappingTable <- do.call(rbind, values)
colnames(mappingTable) <- c("NAMES", "HMDB", "KEGG", "METLIN")
metaboliteMappingTable <- as.data.table(mappingTable)
save(metaboliteMappingTable, file = "metaboliteMappingTable.rda")
