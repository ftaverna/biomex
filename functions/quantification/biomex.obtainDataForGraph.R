#' Obtain the data for the graph.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param dataMatrix The metadata matrix data.table.
#' @param groupVariable The grouping variable.
#' @param feature The feature selected.
#' @observations groupVariable The observations selected.
#' @observations dataMatrixAsInput The type of data matrix used as input.
#'
#' @return A list with: normal data, aggregated data, the density data, the feature name, the observations selected and the observations order.
biomex.obtainDataForGraph <- function(dataMatrix, metadataMatrix, groupVariable, feature, observations, dataMatrixAsInput)
{
  if (feature == "") return (NULL) # Don't return the error message, for a smoother user experience.
  
  observationsOrder <- observations
  
  # Obtain the correct observations based on the matrix type
  if (dataMatrixAsInput %in% c("Normal", "Engineered") && observations != "")
  {
    originalObservations <- biomex.getObservationNames(dataMatrix = dataMatrix)
    metadataMatrix <- biomex.getDataTableSubset(dataTable = metadataMatrix, rows = originalObservations)
    observations <- metadataMatrix[metadataMatrix[[groupVariable]] %in% observations, "Observation"][[1]]
  }
  
  if ("" %in% observations) observations <- biomex.getObservationNames(dataMatrix = dataMatrix)
  
  # Obtain values
  metadataMatrix <- biomex.getDataTableSubset(dataTable = metadataMatrix, rows = observations)
  subsetDataMatrix <- biomex.getDataTableSubset(dataTable = dataMatrix, columns = observations)
  subsetDataMatrix <- biomex.transposeDataMatrix(subsetDataMatrix, isAllNumeric = TRUE)
  groups <- metadataMatrix[[groupVariable]]
  setnames(subsetDataMatrix, colnames(subsetDataMatrix), "Value")
  subsetDataMatrix$Group <- groups
  
  # Aggregation
  meanAggregation <- subsetDataMatrix[, lapply(.SD, mean), by = "Group"]
  sdAggregation <- subsetDataMatrix[, lapply(.SD, sd), by = "Group"]
  if ("" %in% observationsOrder) observationsOrder <- meanAggregation$Group
  
  # Get results
  values <- subsetDataMatrix$Value
  groups <- subsetDataMatrix$Group
  aggregatedValues <- meanAggregation$Value
  aggregatedGroups <- meanAggregation$Group
  standardDeviations <- sdAggregation$Value
  standardDeviations[is.na(standardDeviations)] <- 0 # When you have one observation, you don't have a standard deviation
  standardError <- standardDeviations / sqrt(as.numeric(table(subsetDataMatrix$Group)[observationsOrder]))
  
  # Calculate how many times the feature is detected (per group)
  countZero <- function(x) { return (sum(x == 0))}
  countNotZero <- function(x) { return (sum(x != 0))}
  aggregateZero <- subsetDataMatrix[, lapply(.SD, countZero), by = "Group"][[2]]
  aggregateNotZero <- subsetDataMatrix[, lapply(.SD, countNotZero), by = "Group"][[2]]
  
  # Calculate density plot
  densityList <- list()
  for (group in aggregatedGroups)
  {
    data <- subsetDataMatrix[Group %in% group, "Value"][[1]]
        
    if (length(data) > 1) dens <- density(data)
    else dens <- list(x = 0, y = 0) # No density with one observation
        
    densityList[[group]] <- data.table(Group = group, x = dens$x, y = dens$y)
  }
  
  # Refine results
  normalData <- data.table(Observation = observations, Group = groups, Value = values)
  aggregatedData <- data.table(Group = aggregatedGroups, Value = aggregatedValues, SD = standardDeviations, SE = standardError, Detected = aggregateNotZero, NotDetected = aggregateZero)
  densityData <- do.call(rbind, densityList)
  
  # Order data to match the user ordering
  normalData <- normalData[order(match(normalData$Group, observationsOrder))]
  aggregatedData <- aggregatedData[order(match(aggregatedData$Group, observationsOrder))]
  densityData <- densityData[order(match(densityData$Group, observationsOrder))]

  resultList <- list(normalData = normalData, aggregatedData = aggregatedData, densityData = densityData, feature = feature, observations = observations, observationsOrder = observationsOrder, observationVariable = groupVariable)
  
  return (resultList)
}