#' Save the selected variables to file.
#' 
#' @param global The global variables.
#' @param variablesToSave The names of the variables to save.
#' @param filePath The path (filename included) where to save the variables.
#' @param compress Whether to compress or not the file.
#' @param checkErrors Whether to check or not for errors during saving.
biomex.saveVariables <- function(global, variablesToSave, filePath, compress = FALSE, checkErrors = TRUE)
{
  print(paste0("Saving variables ", basename(filePath)))
  
  if (!is.null(global))
  {
    globalList <- isolate(reactiveValuesToList(global, all.names = TRUE))
    
    if (variablesToSave[1] != "All")
    {
      globalList <- globalList[variablesToSave]
      
      if (all(is.na(names(globalList))))
        globalList <- NULL
    }
  }
  else
    globalList <- NULL
  
  allVariables <- list(global = globalList)
  
  # Save
  if (checkErrors == FALSE)
  {
    save(allVariables, file = filePath, compress = compress)
  }
  else
  {
    tryCatch({
      save(allVariables, file = filePath, compress = compress)
    }, error = function(err) {
      print(err)
    })
  }
  
  # Remove unwanted variables
  rm(allVariables)
  rm(globalList)
}