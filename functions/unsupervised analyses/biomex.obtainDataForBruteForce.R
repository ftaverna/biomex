#' Obtain the correct data before starting the brute force analysis
#'
#' @param dataMatrixNormalized The normalized data matrix data.table.
#' @param dataMatrixEngineered The engineered data matrix data.table.
#' @param type The type of brute force analysis to perform.
#' @param mappingTable The mapping table.
#' @param drDataMatrixAsInput Whether to use the engineered data matrix or not.
#' @param drDataMatrixNormalizedGrouped The grouped normalized data matrix data.table.
#' @param drDataMatrixEngineeredGrouped The grouped engineered data matrix data.table.
#' @param drColorCoding The selected features in the brute force tab.
#' @param drScoresMatrix The dimensionality reduction scores matrix.
#' @param testRun Whether to perform a test run or not.
#' @param converFeatures Whether or not to convert the original feature names
#' 
#' @return A list with the results based on the brute force analysis to be performed. A list with an error message if it fails.
biomex.obtainDataForBruteForce <- function(dataMatrixNormalized, dataMatrixEngineered, type, mappingTable,
                                           drDataMatrixAsInput, drDataMatrixNormalizedGrouped, drDataMatrixEngineeredGrouped, drColorCoding, drScoresMatrix, # Dimensionality reduction
                                           testRun, converFeatures)   
{
  if (type == "Dimensionality reduction")
  {
    observationsSelected <- drScoresMatrix$Observation
    
    if (drDataMatrixAsInput %in% c("Normal", "Engineered"))
    {
      if (drColorCoding == "Original feature") dataMatrix <- dataMatrixNormalized
      if (drColorCoding == "Engineered feature") dataMatrix <- dataMatrixEngineered
      dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = dataMatrix, columns = observationsSelected)
    }
    
    if (drDataMatrixAsInput %in% c("Normal grouped", "Engineered grouped"))
    {
      if (drColorCoding == "Original feature") dataMatrix <- copy(drDataMatrixNormalizedGrouped) # We need to make a deep copy manually, to avoid any side effects
      if (drColorCoding == "Engineered feature") dataMatrix <- copy(drDataMatrixEngineeredGrouped) # We need to make a deep copy manually, to avoid any side effects
    }
    
    # Convert the feature names
    if (drColorCoding == "Original feature" && converFeatures == TRUE)
    {
      features <- biomex.getFeatureNames(dataMatrix = dataMatrix, mappingTable = mappingTable, fromType = "Original", toType = "Name")
      dataMatrix[, Feature := features]
    }
    
    if (is.null(dataMatrix)) return (list(errorMessage = "The data matrix is empty."))
    if (is.null(drScoresMatrix)) return (list(errorMessage = "The score matrix is empty."))
    
    if (testRun == TRUE) dataMatrix <- dataMatrix[sample(1:nrow(dataMatrix), 1)] # Select one random feature
    
    resultList <- list(dataMatrix = dataMatrix, scoresMatrix = drScoresMatrix)
  }
  
  return (resultList)
}