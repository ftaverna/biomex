#' Perform PCA.
#' 
#' @param dataMatrix The data matrix data.table. It must be already in the correct format (features as columns, no observations).
#' @param dimensions The number of dimensions of the PCA.
#' @param accurateMode If you want to use the normal PCA or not. If set to TRUE, the normal PCA is used, but it is extremely slow.
#'
#' @return A list with the results: the score matrix, the loadings matrix and the variance explained by each PC. NULL if it fails.
biomex.performPCA <- function(dataMatrix, dimensions, accurateMode = FALSE)
{
  # Slow but accurate PCA
  if (accurateMode == TRUE) 
  {
    pca <- tryCatch({
      pca <- prcomp(dataMatrix, scale. = FALSE, center = FALSE)
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    # If it fails, return NULL
    if (is.null(pca)) return (NULL)
    
    # Make the result format consistent
    scoresMatrix <- as.data.table(pca$x)
    
    # If the selected dimensions are greater than the ones found, change it
    if (dimensions > ncol(scoresMatrix)) dimensions <- ncol(scoresMatrix)
    
    scoresMatrix <- scoresMatrix[, 1:dimensions, with = FALSE]
    loadingsMatrix <- as.data.table(pca$rotation)
    loadingsMatrix <- loadingsMatrix[, 1:dimensions, with = FALSE]
    varianceExplained <- pca$sdev ^ 2 / sum(pca$sdev ^ 2)
    varianceExplained <- signif(varianceExplained * 100, 4)
  }
  # Fast but less accurate PCA
  else 
  {
    pca <- tryCatch({
      pca <- flashpca(as.matrix(dataMatrix), ndim = dimensions, stand = "none", do_loadings = TRUE, divisor = "none")
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    # If it fails, return NULL
    # FIX: HACK: the tryCatch is not working properly, so we need to check the class of pca[[1]] to decide whether it failed or not
    if (class(pca[[1]]) == "character") return (NULL)
    
    # Make the result format consistent
    scoresMatrix <- as.data.table(pca$projection)
    loadingsMatrix <- as.data.table(pca$loadings)
    # s <- sum(dataMatrix ^ 2) / (nrow(dataMatrix) - 1)
    squaredSum <- function(values) { return (sum(values ^ 2)) }
    s <- sum(sapply(dataMatrix, squaredSum)) / (nrow(dataMatrix) - 1) 
    varianceExplained <- pca$values / (nrow(dataMatrix) - 1) / s
    varianceExplained <- signif(varianceExplained * 100, 4)
  }
  
  resultList <- list(scoresMatrix = scoresMatrix, loadingsMatrix = loadingsMatrix, varianceExplained = varianceExplained)
  
  return (resultList)
}