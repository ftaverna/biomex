#' Check the consistency between the data matrix and the metadata matrix matrix.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param metadataMatrix The metadata matrix data.table.
#' @param experimentInformation The list which contains the experiment information.
#' 
#' @return A list which contains: the updated metadata matrix, the number of observations removed from the metadata, the number of observations added to the metadata.
biomex.checkDataMetadataConsistency <- function(dataMatrix, metadataMatrix, experimentInformation)
{
  # Obtain observation names from the data matrix
  if (experimentInformation$Type == "Metabolomics") dataObservations <- colnames(dataMatrix)[-c(1, 2)]
  else dataObservations <- colnames(dataMatrix)[-1]
  
  # If no metadata matrix is present, create an empty one
  if (is.null(metadataMatrix)) metadataMatrix <- data.table(Observation = dataObservations, Experiment = experimentInformation$Name)
  metadataObservations <- metadataMatrix$Observation
  
  # The data matrix is taken as the "ground truth", so we modify the metadata matrix with respect to the data matrix (and not the other way around)
  # Remove Observations from the metadata that are not in the data
  removeIndex <- which(!metadataObservations %in% dataObservations)
  removeLength <- length(removeIndex)
  
  if (length(removeIndex) > 0)
  {
    metadataMatrix <- metadataMatrix[-removeIndex]
    metadataObservations <- metadataMatrix$Observation
  }
  
  # Add observations that are in the data that are missing in the metadata
  addIndex <- which(!dataObservations %in% metadataObservations)
  addLength <- length(addIndex)
  
  if (length(addIndex) > 0)
  {
    # Set up the data.table to add
    addObservations <- dataObservations[addIndex]
    toAdd <- data.table(matrix("", ncol = ncol(metadataMatrix), nrow = addLength))
    setnames(toAdd, colnames(toAdd), colnames(metadataMatrix))
    toAdd[, Observation := addObservations]
    metadataMatrix <- rbindlist(list(metadataMatrix, toAdd))
    metadataObservations <- metadataMatrix$Observation
  }
  
  # Match ordering between the data matrix and the metadata matrix
  matchIndex <- order(match(metadataObservations, dataObservations))
  metadataMatrix <- metadataMatrix[matchIndex]
  
  resultList <- list(metadataMatrix = metadataMatrix, removedObservations = removeLength, addedObservations = addLength)
  
  return (resultList)
}