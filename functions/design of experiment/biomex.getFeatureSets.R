#' Obtain the feature sets in the correct format based on the selection.
#' 
#' @param setSelected The type of sets selected.
#' @param setSelectedCustomPath The custom file path with the custom sets to select.
#' @param keggSetNames The name of the KEGG sets selected.
#' @param experimentInformation The list which contains the experiment information.
#' @param featureSets The current feature sets.
#' 
#' @return A list with: the sets in the correct format. A list with an error message if it fails.
biomex.getFeatureSets <- function(setSelected, setSelectedCustomPath, keggSetNames, experimentInformation, featureSets)
{
  # Current sets
  if (setSelected == "Use current (if available)")
  {
    sets <- featureSets
    
    # If the user want to use the current feature set, but it's not available, return an error message
    if (is.null(sets))  return (list(errorMessage = "The current set is empty."))
  }
  
  # KEGG sets
  if (setSelected == "KEGG sets")
  {
    # Genes or metabolites depending on the experiment type
    if (experimentInformation$Type == "Metabolomics")
      sets <- KEGG[[experimentInformation$Organism]]$MetaboliteSet
    else if (experimentInformation$Type %in% c("Transcriptomics", "Proteomics", "Cytometry"))
      sets <- KEGG[[experimentInformation$Organism]]$GeneSet
    
    # Subset based on the KEGG selection
    sets <- sets[names(sets) %in% keggSetNames]
  }
    
  # Custom
  if (setSelected == "Custom")
  {
    sets <- biomex.loadCustomSets(setFilePath = setSelectedCustomPath, experimentInformation = experimentInformation)
    
    # If the file was invalid, return an error message
    if (is.null(sets)) return (list(errorMessage = "Could not load the set file."))
  }
  
  resultList <- list(sets = sets)
  
  return (resultList)
}