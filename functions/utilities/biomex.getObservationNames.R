#' Get the observation names from the data matrix.
#' 
#' @param dataMatrix The data matrix data.table.
#' 
#' @return The observation names.
biomex.getObservationNames <- function(dataMatrix)
{
  # Get observations based on the class of the object
  if ("data.table" %in% class(dataMatrix) || "data.frame" %in% class(dataMatrix)) 
  {
    observations <- colnames(dataMatrix)[-1]
  }
  else if ("dgCMatrix" %in% class(dataMatrix) || "matrix" %in% class(dataMatrix)) 
  {
    observations <- colnames(dataMatrix)
  }
  else 
  {
    observations <- NULL
  }
  
  return (observations)
}