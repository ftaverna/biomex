#' Set the Java Look and Feel based on the operative system.
biomex.setJavaLookAndFeel <- function()
{
  # Get look and feel
  systemLookAndFeel <- J("javax.swing.UIManager", "getSystemLookAndFeelClassName")
  
  # Set windows look and feel if the OS is Windows, otherwise use Nimbus
  if(Sys.info()['sysname'] == "Windows")
  {
    J("javax.swing.UIManager", "setLookAndFeel", "com.sun.java.swing.plaf.windows.WindowsLookAndFeel")
  }
  else
  {
    J("javax.swing.UIManager", "setLookAndFeel", "javax.swing.plaf.nimbus.NimbusLookAndFeel")
  }
}

