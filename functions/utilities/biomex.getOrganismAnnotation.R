#' Get the organism annotation string given the organism name.
#' 
#' @param organism The organism name
#' 
#' @return The annotation string if the organism is supported. NULL otherwise.
biomex.getOrganismAnnotation <- function(organism)
{
  if(organism == "Human")
    annotation <- "org.Hs.eg.db"
  else if(organism == "Mouse")
    annotation <- "org.Mm.eg.db"
  else if(organism == "Rat")
    annotation <- "org.Rn.eg.db"
  else if(organism == "Cow")
    annotation <- "org.Bt.eg.db"
  else if(organism == "Zebrafish")
    annotation <- "org.Dr.eg.db"
  else if(organism == "Pig")
    annotation <- "org.Ss.eg.db"
  else if(organism == "Sheep")
    annotation <- "org.Oa.eg.db"
  else if(organism == "Crab-eating macaque")
    annotation <- "org.Mfascicularis.eg.db"
  else
    annotation <- NULL
  
  return (annotation)
}