#' Function to easily create an empty DT datatable with some text in it.
#' 
#' @param text The text to visualize.
#' 
#' @return The DT datatable object.
biomex.blankTable <- function(text = "")
{
  df <- data.table(Information = text)
  colnames(df) <- " "
  
  return (df)
}
  