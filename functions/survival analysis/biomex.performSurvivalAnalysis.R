#' Perform the survival analysis
#' 
#' @param dataMatrix The data matri data.table
#' @param metadataMatrix The metadata matrix data.table obtained in the preparation step of the survival analysis.
#' @param mappingTable The mapping table.
#' @param observationsSelected The observations selected in the design of experiment.
#' @param featureType The type of feature selected (original or engineered).
#' @param feature The feature selected.
#' @param quantiles How to split the data (vector of two numbers between 0 and 1)
#' @param excludeMedium Whether or not to exclude the medium observation.
#' 
#' @return A list which contains: the survival modal and the pvalue. One list is returned for each stratification.
biomex.performSurvivalAnalysis <- function(dataMatrix, metadataMatrix, mappingTable, observationsSelected, featureType, feature, quantiles, excludeMedium)
{
  if (feature == "") return (NULL)
  
  # Subset the data matrix
  if (featureType == "Original feature") 
  {
    featureConverted <- biomex.mapFeatures(features = feature, from = "Name", to = "Original", mappingTable = mappingTable)
    dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = dataMatrix, columns = observationsSelected, rows = featureConverted, mappingTable = mappingTable, from = "Original", to = "Name")
  }
  else dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = dataMatrix, columns = observationsSelected, rows = feature)
  
  # Select the stratifications
  stratifications <- metadataMatrix$Stratification
  stratificationsLevels <- levels(factor(unique(stratifications)))
  if (is.null(stratifications)) stratifications <- "No stratification"
  
  resultList <- NULL
  for (stratification in stratificationsLevels)
  {
    # Get the stratification index. If only two samples are present, skip this stratification and go to the next
    indexStratification <- which(stratifications == stratification)
    if(length(indexStratification) <= 2) next 
    
    # Get the data of the feature selected
    observations <- metadataMatrix$Observation[indexStratification]
    data <- as.numeric(dataMatrix[, ..observations])
    metadataMatrixStratification <- biomex.getDataTableSubset(metadataMatrix, rows = observations)
    
    # Calculate median
    median <- median(data)
    
    # Divide the groups based on the median
    group <- rep("", length(data))
    lowerQuantile <- quantile(data, probs = quantiles[1])
    upperQuantile <- quantile(data, probs = quantiles[2])
    
    # When the quantiles are the same, you want include all the data
    if (length(unique(quantiles)) == 1)
    {
      group[data <= lowerQuantile] <- paste0(feature, " low")
      group[data > upperQuantile] <- paste0(feature, " high")
    }
    else 
    {
      group[data < lowerQuantile] <- paste0(feature, " low")
      group[data > upperQuantile] <- paste0(feature, " high")
      group[data >= lowerQuantile & data <= upperQuantile] <- paste0(feature, " medium")
    }
    
    # Check if we have 2 or more groups
    if (length(unique(group)) == 1) return (NULL)
    
    # Add the group to the metadata
    metadataMatrixStratification$Group <- group
    
    # Exclude medium observations (if needed or present)
    if (excludeMedium == TRUE)
    {
      indexNotMedium <- which(group != paste0(feature, " medium"))
      if (length(indexNotMedium) > 0) metadataMatrixStratification <- metadataMatrixStratification[indexNotMedium]
      if (length(unique(metadataMatrixStratification$Group)) == 1) return (NULL)
    }
    
    # Perform survival analysis
    survivalFormula <- formula(Surv(as.numeric(metadataMatrixStratification$Time), event = metadataMatrixStratification$Event) ~ metadataMatrixStratification$Group)
    survivalFit <- do.call(survfit, list(formula = survivalFormula, data = metadataMatrixStratification))
    
    # Calculate p-value
    survivalDiff <- survdiff(survivalFormula, data = metadataMatrixStratification)
    pvalue <- pchisq(survivalDiff$chisq, length(survivalDiff$n) - 1, lower.tail = FALSE)
    
    resultList[[stratification]] <- list(survivalFit = survivalFit, pvalue = pvalue, group = group, feature = feature)
  }
  
  if (is.null(resultList)) return (NULL)
  
  return (resultList)
}
