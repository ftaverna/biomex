#' Filter the features of the data matrix based on the parameters selected.
#' The data matrix passed to the function WILL BE FILTERED BY REFERENCE.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param featureAverageExpressionThreshold The rowMeans threshold to consider a feature expressed. If NULL, filtering is skipped.
#' @param featureExpressionThreshold The threshold to consider a feature expressed. If NULL, filtering is skipped.
#' @param observationPercentageThreshold The percentage of samples that have to express the feature. If NULL, filtering is skipped.
#' 
#' @return A list with: the filtered data matrix, the number of feature filtered after the row means filterting and 
#' the number of features filtered after the feature filtering. A list with an error message if it fails.
biomex.filterLowlyExpressedFeatures <- function(dataMatrix, experimentInformation, featureAverageExpressionThreshold, featureExpressionThreshold, observationPercentageThreshold)
{
  if (experimentInformation$Data != "Raw") return (list(errorMessage = "The filtering can only be performed on 'Raw' data."))
  
  # Get the relevant information and remove the features from the data matrix
  features <- biomex.getFeatureNames(dataMatrix = dataMatrix)
  observations <- biomex.getObservationNames(dataMatrix = dataMatrix)
  dataMatrix[, Feature := NULL]
  
  # For bulk RNA-seq data, the filtering must be done on CPM.
  if (experimentInformation$Technology == "RNA-seq")
  {
    # Save original matrix
    dataMatrixOriginal <- dataMatrix
    
    # Calculate CPM
    dataMatrix <- DGEList(dataMatrixOriginal)
    dataMatrix <- as.data.table(edgeR::cpm(dataMatrix))
    
    # Re-add the Feature column to the original data matrix
    dataMatrixOriginal[, Feature := features]
    setcolorder(dataMatrixOriginal, c("Feature", observations))
  }
  
  # 1. Row means filtering (single cell RNA-seq)
  # 2. Percentage of samples that have to express a feature filtering (bulk RNA-seq)
  rowMeanFiltered <- 0
  presenceFiltered <- 0
  indexAverage <- integer(0)
  indexPresence <- integer(0)
  
  # Single cell RNA-seq filtering
  if (!is.null(featureAverageExpressionThreshold) && featureAverageExpressionThreshold > 0 && experimentInformation$Technology == "scRNA-seq")
  {
    indexAverage <- which(rowMeans(dataMatrix) < featureAverageExpressionThreshold)
    rowMeanFiltered <- length(indexAverage)
    
    # if (length(indexAverage) > 0) dataMatrix <- dataMatrix[-indexAverage]
    if (length(indexAverage) > 0) 
    {
      dataMatrix <- biomex.deleteRows(dataTable = dataMatrix, deleteIndex = indexAverage) # The rows are deleted by reference
      features <- features[-indexAverage]
    }
  }
  
  # No features left, return NULL
  if (nrow(dataMatrix) == 0) return (list(errorMessage = "The filtering was too strict."))
  
  
  # Bulk RNA-seq filtering
  if (!is.null(featureExpressionThreshold) && !is.null(observationPercentageThreshold) && featureExpressionThreshold > 0 && observationPercentageThreshold > 0  && experimentInformation$Technology == "RNA-seq")
  {
    observationThreshold <- as.integer(((ncol(dataMatrix) - 1) * observationPercentageThreshold) / 100) # Truncated to integer
    indexPresence <- which(rowSums(dataMatrix >= featureExpressionThreshold) >= observationThreshold) # Index of correctly expressed features
    presenceFiltered <- nrow(dataMatrix) - length(indexPresence)
    indexAbsent <- setdiff(dataMatrix[, .I], indexPresence) # Get the index of the not expressed features
    
    # if (length(indexAbsent) > 0) dataMatrix <- dataMatrix[-indexAbsent]
    if (length(indexAbsent) > 0)
    {
      dataMatrix <- biomex.deleteRows(dataTable = dataMatrix, deleteIndex = indexAbsent) # The rows are deleted by reference
      features <- features[-indexAbsent]
    }
  }
  
  # No features left, return NULL
  if (nrow(dataMatrix) == 0) return (list(errorMessage = "The filtering was too strict."))
  
  # # Restore RNA-seq matrix without CPM values
  # if (experimentInformation$Technology == "RNA-seq")
  # {
  #   features <- biomex.getFeatureNames(dataMatrix = dataMatrix)
  #   dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = dataMatrixOriginal, rows = features)
  # }
  
  # Restore RNA-seq matrix without CPM values
  if (experimentInformation$Technology == "RNA-seq")
  {
    featuresOriginal <- biomex.getFeatureNames(dataMatrix = dataMatrixOriginal)
    indexRemoved <- which(!featuresOriginal %in% features)
    
    if (length(indexRemoved) > 0) dataMatrix <- biomex.deleteRows(dataTable = dataMatrixOriginal, deleteIndex = indexRemoved) # The rows are deleted by reference
  }
  
  # Add the Feature column back
  dataMatrix[, Feature := features]
  setcolorder(dataMatrix, c("Feature", observations))
  
  resultList <- list(dataMatrix = dataMatrix, rowMeanFiltered = rowMeanFiltered, presenceFiltered = presenceFiltered)
  
  return (resultList)
}