#' Compress a data.table to a sparse matrix.
#' The data used as input is PARTIALLY MODIFIED BY REFERENCE. Be careful of the data you pass to this function.
#'
#' @param dataMatrix The decompressed data matrix data.table.
#'
#' @return The compressed data matrix (dgCMatrix).
biomex.compressMatrix <- function(dataMatrix)
{
  # If it's already compressed, do not compress
  if ("dgCMatrix" %in% class(dataMatrix)) return (dataMatrix)
  
  dataMatrix <- biomex.toSparseMatrix(dataMatrix = dataMatrix)
  
  # Clean memory
  biomex.callGC()
  
  return (dataMatrix)
}
