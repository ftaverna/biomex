#' Transpose the data matrix.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param isAllNumeric Specify whether the data matrix is all numeric (no Feature or Observation column present) (default FALSE).
#' @param dropFirstColumn Specify whether to drop the first column after the transposition (default FALSE).
#'
#' @return The transpose data matrix
biomex.transposeDataMatrix <- function(dataMatrix, isAllNumeric = FALSE, dropFirstColumn = FALSE)
{
  firstColumn <- colnames(dataMatrix)[1]
  if (isAllNumeric == TRUE) firstColumn <- "" 
  
  dataMatrixOriginal <- dataMatrix
  
  if (firstColumn == "Feature") 
  {
    features <- biomex.getFeatureNames(dataMatrix = dataMatrix)
    observations <- biomex.getObservationNames(dataMatrix = dataMatrix)
    dataMatrixOriginal[, Feature := NULL]
  }
  else if (firstColumn == "Observation") 
  {
    features <- colnames(dataMatrix)[-1]
    observations <- dataMatrix[[1]]
    dataMatrixOriginal[, Observation := NULL]
  }
  else if (firstColumn == "Variable") 
  {
    observations <- colnames(dataMatrix)[-1]
    variables <- dataMatrix[[1]]
    dataMatrixOriginal[, Variable := NULL]
  }
  
  dataMatrix <- transpose(dataMatrixOriginal)
  
  # Re-add features and observation
  if (firstColumn == "Feature")
  {
    setnames(dataMatrix, colnames(dataMatrix), features)
    
    if (dropFirstColumn == FALSE)
    {
      dataMatrix[, Observation := observations]
      setcolorder(dataMatrix, c("Observation", features))
    }
    
    # Re-add feature to the original data matrix by reference to avoid any side effects
    dataMatrixOriginal[, Feature := features]
    setcolorder(dataMatrixOriginal, c("Feature", observations))
    
  }
  else if (firstColumn == "Observation") 
  {
    setnames(dataMatrix, colnames(dataMatrix), observations)
    
    if (dropFirstColumn == FALSE)
    {
      dataMatrix[, Feature := features]
      setcolorder(dataMatrix, c("Feature", observations))
    }
    
    # Re-add feature to the original data matrix by reference to avoid any side effects
    dataMatrixOriginal[, Observation := observations]
    setcolorder(dataMatrixOriginal, c("Observation", features))
  }
  else if (firstColumn == "Variable")
  {
    setnames(dataMatrix, colnames(dataMatrix), variables)
    
    if (dropFirstColumn == FALSE)
    {
      dataMatrix[, Variable := observations]
      setcolorder(dataMatrix, c("Variable", variables))
    }
    
    # Re-add feature to the original data matrix by reference to avoid any side effects
    dataMatrixOriginal[, Variable := variables]
    setcolorder(dataMatrixOriginal, c("Variable", observations))
  }

  return (dataMatrix)
}