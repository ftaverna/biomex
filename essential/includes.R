# This file contains the paths to all the R files that must be included for BIOMEX to work.
# Everytime a file is added that is needed by BIOMEX, then it must go here.

# *** Logic ====
# Root level ====
source("logic/logic_collection.R")

# Server ====
source("logic/server/logic_server.R")

# Modals ====
source("logic/modals/logic_modals.R")

# Landing ====
source("logic/landing/logic_landing.R")

# Menu ====
source("logic/menu/logic_menu.R")
source("logic/menu/logic_header.R")

# Prepare data ====
source("logic/prepare experiments/logic_prepareexperiments.R")

# Manage data ====
source("logic/manage experiments/logic_manageexperiments.R")

# Data ====
source("logic/data/logic_data.R")

# Metadata ====
source("logic/metadata/logic_metadata.R")

# Data pretreatment ====
source("logic/data pretreatment/logic_datapretreatment.R")

# Design of experiment ====
source("logic/design of experiment/biomex.designOfExperimentLogic.R")
source("logic/design of experiment/biomex.observationSelectionLogic.R")
source("logic/design of experiment/biomex.featureSelectionLogic.R")
source("logic/design of experiment/biomex.setSelectionLogic.R")
source("logic/design of experiment/biomex.additionalFilteringLogic.R")
source("logic/design of experiment/biomex.storeAndLoadResultsLogic.R")

# Feature engineering ====
source("logic/feature engineering/logic_featureengineering.R")

# Quantification ====
source("logic/quantification/logic_graph.R")
source("logic/quantification/logic_metadatagraph.R")

# Unsupervised analyses ====
source("logic/unsupervised analyses/logic_dimensionalityreduction.R")
source("logic/unsupervised analyses/logic_clustering.R")
source("logic/unsupervised analyses/logic_heatmap.R")
source("logic/unsupervised analyses/logic_bruteforce.R")

# Supervised analyses ====
source("logic/supervised analyses/logic_differentialanalysis.R")
source("logic/supervised analyses/logic_competitivesetenrichmentanalysis.R")
source("logic/supervised analyses/logic_selfcontainedsetenrichmentanalysis.R")
source("logic/supervised analyses/logic_pathwaymapping.R")
source("logic/supervised analyses/logic_markersetanalysis.R")

# Single cell analyses ====
source("logic/single cell analyses/logic_pseudotimeanalysis.R")
source("logic/single cell analyses/logic_clusterprediction.R")
source("logic/single cell analyses/logic_trajectoryanalysis.R")

# Survival analysis ====
source("logic/survival analysis/logic_survivalanalysis.R")

# Machine learning ====
source("logic/machine learning/logic_machinelearning.R")

# Meta analysis ====
source("logic/meta analysis/logic_metaanalysis.R")

# Cluster similarity ====
source("logic/cluster similarity/logic_clustersimilarity.R")

# *** Interface ====
# Root level ====
source("interface/interface_collection.R")

# Modals ====
source("interface/modals/interface_modals.R")

# Menu ====
source("interface/menu/interface_menu.R")
source("interface/menu/interface_header.R")

# Landing ====
source("interface/landing/interface_landing.R")

# Prepare data ====
source("interface/prepare experiments/interface_prepareexperiments.R")

# Manage data ====
source("interface/manage experiments/interface_manageexperiments.R")

# Data ====
source("interface/data/interface_data.R")

# Metadata ====
source("interface/metadata/interface_metadata.R")

# Data pretreatment ====
source("interface/data pretreatment/interface_datapretreatment.R")

# Design of experiment ====
source("interface/design of experiment/biomex.designOfExperimentInterface.R")
source("interface/design of experiment/biomex.observationSelectionInterface.R")
source("interface/design of experiment/biomex.featureSelectionInterface.R")
source("interface/design of experiment/biomex.setSelectionInterface.R")
source("interface/design of experiment/biomex.additionalFilteringInterface.R")
source("interface/design of experiment/biomex.storeAndLoadResultsInterface.R")

# Feature engineering ====
source("interface/feature engineering/interface_featureengineering.R")

# Quantification ====
source("interface/quantification/interface_graph.R")
source("interface/quantification/interface_metadatagraph.R")

# Unsupervised analyses ====
source("interface/unsupervised analyses/interface_dimensionalityreduction.R")
source("interface/unsupervised analyses/interface_clustering.R")
source("interface/unsupervised analyses/interface_heatmap.R")
source("interface/unsupervised analyses/interface_bruteforce.R")

# Supervised analyses ====
source("interface/supervised analyses/interface_differentialanalysis.R")
source("interface/supervised analyses/interface_competitivesetenrichmentanalysis.R")
source("interface/supervised analyses/interface_selfcontainedsetenrichmentanalysis.R")
source("interface/supervised analyses/interface_pathwaymapping.R")
source("interface/supervised analyses/interface_markersetanalysis.R")

# Single cell analyses ====
source("interface/single cell analyses/interface_pseudotimeanalysis.R")
source("interface/single cell analyses/interface_clusterprediction.R")
source("interface/single cell analyses/interface_trajectoryanalysis.R")

# Survival analysis ====
source("interface/survival analysis/interface_survivalanalysis.R")

# Machine learning ====
source("interface/machine learning/interface_machinelearning.R")

# Meta analysis ====
source("interface/meta analysis/interface_metaanalysis.R")

# Cluster similarity ====
source("interface/cluster similarity/interface_clustersimilarity.R")

# **** Functions ====
# Menu ====
source("functions/menu/biomex.toggleMenuElement.R")
source("functions/menu/biomex.toggleSubmenuElement.R")
source("functions/menu/biomex.menuInterfaceHandling.R")

# Session ====
source("functions/session/biomex.generateSessionID.R")
source("functions/session/biomex.createSessionDirectory.R")
source("functions/session/biomex.deleteSessionDirectory.R")

# Utilities ====
source("functions/utilities/biomex.directoryInput.R")
source("functions/utilities/biomex.chooseDir.R")
source("functions/utilities/biomex.chooseFile.R")
source("functions/utilities/biomex.chooseFiles.R")
source("functions/utilities/biomex.readFile.R")
source("functions/utilities/biomex.resetGlobalVariables.R")
source("functions/utilities/biomex.resetInterfaceStartup.R")
source("functions/utilities/biomex.removeExtension.R")
source("functions/utilities/biomex.getOrganismAnnotation.R")
source("functions/utilities/biomex.getMetadataNames.R")
source("functions/utilities/biomex.getObservationNames.R")
source("functions/utilities/biomex.getFeatureNames.R")
source("functions/utilities/biomex.getSetNames.R")
source("functions/utilities/biomex.getValidChoice.R")
source("functions/utilities/biomex.createProgressObject.R")
source("functions/utilities/biomex.validateNumericInput.R")
source("functions/utilities/biomex.validateSelectizeInputMultiple.R")
source("functions/utilities/biomex.validateCheckboxGroupInput.R")
source("functions/utilities/biomex.getColorPalette.R")
source("functions/utilities/biomex.getSymbols.R")
source("functions/utilities/biomex.mapFeatures.R")
source("functions/utilities/biomex.obtainCorrectDataMatrix.R")
source("functions/utilities/biomex.checkHash.R")
source("functions/utilities/biomex.makeSettingsConsistent.R")
source("functions/utilities/biomex.makeOldDoEExperimentCompatible.R")
source("functions/utilities/biomex.returnCurrentPlot.R")
source("functions/utilities/biomex.createRecentExperimentsTable.R")
source("functions/utilities/biomex.updateRecentExperimentsTable.R")
source("functions/utilities/biomex.handleSystemPath.R")
source("functions/utilities/bsModalCustom.R")
source("functions/utilities/biomex.blankPlot.R")
source("functions/utilities/biomex.blankTable.R")
source("functions/utilities/biomex.blankHandsontable.R")
source("functions/utilities/biomex.checkVariableInUse.R")
source("functions/utilities/biomex.getPredefinedColors.R")
source("functions/utilities/biomex.getCustomColors.R")
source("functions/utilities/biomex.areColors.R")
source("functions/utilities/biomex.detectApplicationDataDirectory.R")
source("functions/utilities/biomex.setJavaLookAndFeel.R")
source("functions/utilities/biomex.initializeJava.R")
source("functions/utilities/biomex.removeVariables.R")
source("functions/utilities/biomex.callGC.R")
source("functions/utilities/biomex.checkNA.R")


# Data ====
source("functions/data/biomex.getDataInformation.R")
source("functions/data/biomex.getDataInformationSubset.R")
source("functions/data/biomex.getSelectedMatrix.R")

# Data manipulation ====
source("functions/data manipulation/biomex.divideByColumn.R")
source("functions/data manipulation/biomex.setDataTableRow.R")
source("functions/data manipulation/biomex.normalizeData.R")
source("functions/data manipulation/biomex.getDataTableSubset.R")
source("functions/data manipulation/biomex.getDataMatrixSubset.R")
source("functions/data manipulation/biomex.transformData.R")
source("functions/data manipulation/biomex.scaleData.R")
source("functions/data manipulation/biomex.groupDataMatrix.R")
source("functions/data manipulation/biomex.fastGrouping.R")
source("functions/data manipulation/biomex.fisherPvalue.R")
source("functions/data manipulation/biomex.transposeDataMatrix.R")
source("functions/data manipulation/biomex.compressMatrix.R")
source("functions/data manipulation/biomex.decompressMatrix.R")
source("functions/data manipulation/biomex.scaleVector.R")
source("functions/data manipulation/biomex.deleteRows.R")
source("functions/data manipulation/biomex.mergeFeatures.R")
source("functions/data manipulation/biomex.setRowOrderByIndex.R")
source("functions/data manipulation/biomex.toSparseMatrix.R")

# Prepare data ====
source("functions/prepare data/biomex.handleInterfacePrepareData.R")
source("functions/prepare data/biomex.combineDataSelection.R")
source("functions/prepare data/biomex.validateCombinedAnnotation.R")
source("functions/prepare data/biomex.createOutputDirectory.R")
source("functions/prepare data/biomex.getDataMatrix.R")
source("functions/prepare data/biomex.getMetadataMatrix.R")
source("functions/prepare data/biomex.checkDataMetadataConsistency.R")
source("functions/prepare data/biomex.cleanDataMatrix.R")
source("functions/prepare data/biomex.createMappingTable.R")
source("functions/prepare data/biomex.findMitochondrialGenes.R")
source("functions/prepare data/biomex.getCustomAnnotationDatabase.R")
source("functions/prepare data/biomex.checkNegativeProcessing.R")

# Data pretreatment ====
source("functions/data pretreatment/biomex.subsetObservations.R")
source("functions/data pretreatment/biomex.filterLowlyExpressedFeatures.R")
source("functions/data pretreatment/biomex.filterByMitochondrialExpression.R")
source("functions/data pretreatment/biomex.noiseFiltering.R")
source("functions/data pretreatment/biomex.normalizeByFeature.R")
source("functions/data pretreatment/biomex.injectionOrderRegression.R")
source("functions/data pretreatment/biomex.pretreatmentLoessRegression.R")
source("functions/data pretreatment/biomex.pretreatmentRegressionPlot.R")
source("functions/data pretreatment/biomex.imputeData.R")
source("functions/data pretreatment/biomex.batchCorrection.R")
source("functions/data pretreatment/biomex.checkForNegativeValues.R")

# Design of experiment ====
source("functions/design of experiment/biomex.validateDesignOfExperiment.R")
source("functions/design of experiment/biomex.getKEGGSetNames.R")
source("functions/design of experiment/biomex.getFeatureSets.R")
source("functions/design of experiment/biomex.loadCustomSets.R")
source("functions/design of experiment/biomex.getObservationSubset.R")
source("functions/design of experiment/biomex.getFeatureSubset.R")
source("functions/design of experiment/biomex.additionalFiltering.R")
source("functions/design of experiment/biomex.getMetabolicFeatures.R")
source("functions/design of experiment/biomex.getTranscriptionFeatures.R")
source("functions/design of experiment/biomex.getTransporterFeatures.R")
source("functions/design of experiment/biomex.getCellSurfaceFeatures.R")

# Feature engineering ====
source("functions/feature engineering/biomex.featureEngineering.R")
source("functions/feature engineering/biomex.createEmptyFeatureEngineeringResults.R")

# Quantification ====
source("functions/quantification/biomex.obtainDataForGraph.R")
source("functions/quantification/biomex.obtainDataForMetadataGraph.R")

# Unsupervised analyses ====
source("functions/unsupervised analyses/biomex.dimensionalityReduction.R")
source("functions/unsupervised analyses/biomex.performPCA.R")
source("functions/unsupervised analyses/biomex.createHeatmap.R")
source("functions/unsupervised analyses/biomex.performHeatmapClustering.R")
source("functions/unsupervised analyses/biomex.pvclust.R")
source("functions/unsupervised analyses/biomex.performMarkerSetAnalysis.R")
source("functions/unsupervised analyses/biomex.obtainDataForBruteForce.R")
source("functions/unsupervised analyses/biomex.performBruteForce.R")
source("functions/unsupervised analyses/biomex.plotBruteForceColorDimensionalityReduction.R")
source("functions/unsupervised analyses/biomex.automatedClustering.R")
source("functions/unsupervised analyses/biomex.obtainDataForAutomatedClustering.R")

# Supervised analyses ====
source("functions/supervised analyses/biomex.differentialAnalysis.R")
source("functions/supervised analyses/biomex.getModelFormula.R")
source("functions/supervised analyses/biomex.performCompetitiveSetEnrichment.R")
source("functions/supervised analyses/biomex.performSelfContainedSetEnrichment.R")
source("functions/supervised analyses/biomex.performPathwayMapping.R")
source("functions/supervised analyses/biomex.mixedModel.R")

# Single cell analyses ====
source("functions/single cell analyses/biomex.performPseudotimeAnalysisMonocle.R")
source("functions/single cell analyses/biomex.performPseudotimeAnalysisScorpius.R")
source("functions/single cell analyses/biomex.drawTrajectoryPlotMonocle.R")
source("functions/single cell analyses/biomex.drawTrajectoryPlotScorpius.R")
source("functions/single cell analyses/biomex.performTrajectoryAnalysis.R")
source("functions/single cell analyses/biomex.fitEllipse.R")
source("functions/single cell analyses/biomex.calculatePseudotime.R")
source("functions/single cell analyses/biomex.calculateEllipseDistance.R")
source("functions/single cell analyses/biomex.obtainDataToPredict.R")
source("functions/single cell analyses/biomex.performClusterPrediction.R")
source("functions/single cell analyses/biomex.obtainSankeyData.R")
source("functions/single cell analyses/biomex.pseudotimeQuantileGrouping.R")
source("functions/single cell analyses/biomex.addPseudotimeToMetadata.R")

# Survival analysis ====
source("functions/survival analysis/biomex.setupSurvivalAnalysis.R")
source("functions/survival analysis/biomex.performSurvivalAnalysis.R")
source("functions/survival analysis/biomex.ggsurv.R")

# Machine learning ====
source("functions/machine learning/biomex.perfomMachineLearningFeatureSelection.R")
source("functions/machine learning/biomex.performMachineLearningTraining.R")
source("functions/machine learning/rangerFuncs.R")
source("functions/machine learning/biomex.getConfusionMatrix.R")
source("functions/machine learning/biomex.getClassMetrics.R")
source("functions/machine learning/biomex.getVariableImportance.R")

# Meta analysis ====
source("functions/meta analysis/biomex.performMetaAnalysis.R")
source("functions/meta analysis/biomex.metaAnalysisCreateTables.R")

# Cluster similarity ====
source("functions/cluster similarity/biomex.performClusterSimilarityAnalysis.R")
source("functions/cluster similarity/biomex.calculateJaccardIndex.R")
source("functions/cluster similarity/biomex.calculateCongruentGeneScores.R")
source("functions/cluster similarity/biomex.createVennDiagrams.R")
source("functions/cluster similarity/biomex.calculateListOverlap.R")
source("functions/cluster similarity/biomex.getNamesFromClusterSimilarityData.R")

# Save and load ====
source("functions/save and load/biomex.saveVariables.R")
source("functions/save and load/biomex.loadVariables.R")
source("functions/save and load/biomex.saveExperiment.R")
source("functions/save and load/biomex.loadExperiment.R")
source("functions/save and load/biomex.loadShared.R")
source("functions/save and load/biomex.switchExperiment.R")

# *** Decoy (must be last) ====
source("logic/decoy/logic_decoy.R")
source("interface/decoy/interface_decoy.R")


