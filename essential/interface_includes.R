# This file contains the paths to all the R files that must be included for the BIOMEX interface to work.
# Everytime a file is added that is needed by the BIOMEX interface, then it must go here.

# *** Interface ====
# Root level ====
source("interface/interface_collection.R")

# Modals ====
source("interface/modals/interface_modals.R")

# Menu ====
source("interface/menu/interface_menu.R")
source("interface/menu/interface_header.R")

# **** Functions ====
# Utilities ====
source("functions/utilities/biomex.directoryInput.R")
source("functions/utilities/bsModalCustom.R")

# *** Decoy (must be last) ====
source("interface/decoy/interface_decoy.R")
