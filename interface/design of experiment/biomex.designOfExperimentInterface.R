#' Function that handle the design of experiment interface in BIOMEX.
#' 
#' @param id The id.
biomex.designOfExperimentInterface <- function(id)
{
  mimeType <- c("text/csv", "text/comma-separated-values", "text/plain", ".csv")
  
  # IDs
  # Global
  globalSettingsId <- paste0(id, "DoeSettings")
  globalFinalSettingsId <- paste0(id, "DoeFinalSettings")
  globalResultsId <- paste0(id, "DoeResults")
  globalAnalysisSettingsId <- paste0(id, "Settings")
  globalAnalysisFinalSettingsId <- paste0(id, "FinalSettings")
  globalAnalysisResultsId <- paste0(id, "Results")
  globalAnalysisAdditionalResultsId <- paste0(id, "AdditionalResults")
  globalAnalysisPlotId <- paste0(id, "Plot")
  globalAnalysisTableId <- paste0(id, "Table")
  
  # Box id
  doeBoxId <- paste0(id, "DoEBox")
  settingsBoxId <-  paste0(id, "SettingsBox")
  mainId <- paste0(id, "Main")
  sideSettingsId <- paste0(id, "SideSettings")
  sideControlsId <- paste0(id, "SideControls")
  
  # Action buttons
  observationSelectionId <- paste0(id, "DoeObservationSelection")
  featureSelectionId <- paste0(id, "DoeFeatureSelection")
  setSelectionId <- paste0(id, "DoeSetSelection")
  additionalFilteringId <- paste0(id, "DoeAdditionalFiltering")
  performDesignOfExperimentId <- paste0(id, "performDesignOfExperiment")
  informationDesignOfExperimentId <- paste0(id, "DoeInformation")
  resetDesignOfExperimentId <- paste0(id, "DoeReset")
  
  # Modals
  observationSelectionModalId <- paste0(observationSelectionId, "Modal")
  featureSelectionModalId <- paste0(featureSelectionId, "Modal")
  setSelectionModalId <- paste0(setSelectionId, "Modal")
  additionalFilteringModalId <- paste0(additionalFilteringId, "Modal")
  informationDesignOfExperimentModalId <- paste0(informationDesignOfExperimentId, "Modal")
  
  # Information table
  informationTableId <- paste0(id, "DoeInformationTable")
  
  # Useful variables
  performedId <- paste0(id, "DoePerformed")
  performedAnalysisId <- paste0(id, "Performed")
  triggerId <- paste0(id, "DoeTrigger")
  triggerAnalysisId <- paste0(id, "Trigger")
  
  tags$div(
    box(
      title = "Design of experiment",
      width = NULL,
      collapsible = TRUE,
      collapsed = FALSE,
      id = doeBoxId,
      
      hidden(actionButton(observationSelectionId, label = tags$b("Observation selection"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")),
      hidden(actionButton(featureSelectionId, label = tags$b("Feature selection"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")),
      hidden(actionButton(setSelectionId, label = tags$b("Set selection"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")),
      hidden(actionButton(additionalFilteringId, label = tags$b("Additional filtering"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")),
    
      tags$div(style="display:inline-block; width: 70%; float: left", actionButton(performDesignOfExperimentId, tags$b("Update"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E")),
      tags$div(style="display:inline-block; width: 15%; float: right", actionButton(informationDesignOfExperimentId, NULL, icon = icon("info"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")),
      tags$div(style="display:inline-block; width: 15%; float: right", actionButton(resetDesignOfExperimentId, NULL, icon = icon("repeat"), width = "100%", style = "color: #fff; background-color: #95A5A6; border-color: #7F8C8D"))
    ),
    
    # Modals
    # Observation selection
    bsModal(observationSelectionModalId, "Observation selection", NULL, size = "large", trigger = observationSelectionId, close.button = TRUE, 
      biomex.observationSelectionInterface(id = paste0(id, "Doe"))
    ),
    
    # Feature selection
    bsModal(featureSelectionModalId, "Feature selection", NULL, size = "large", trigger = featureSelectionId, close.button = TRUE, 
      biomex.featureSelectionInterface(id = paste0(id, "Doe"))
    ),
      
    # Set selection
    bsModal(setSelectionModalId, "Set selection", NULL, size = "large", trigger = setSelectionId, close.button = TRUE, 
      biomex.setSelectionInterface(id = paste0(id, "Doe"))
    ),
    
    # Additional filtering
    bsModal(additionalFilteringModalId, "Additional filtering", NULL, size = "large", trigger = additionalFilteringId, close.button = TRUE, 
      biomex.additionalFilteringInterface(id = paste0(id, "Doe"))
    ),
    
    # Information table
    bsModal(informationDesignOfExperimentModalId, "Design of experiment information", NULL, size = "large", trigger = informationDesignOfExperimentId, close.button = TRUE, 
      withSpinner(shiny::dataTableOutput(informationTableId), type = 7)
    )
  )
}