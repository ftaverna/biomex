#' The logic of the Differential Analysis tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicDifferentialAnalysis <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "da", 
    allowObservations = TRUE, allowFeatures = TRUE , allowSets = FALSE, allowAdditionalFiltering = FALSE, allowGrouping = FALSE)
  
  # **** Store and load results ====
  biomex.storeAndLoadResultsLogic(input, output, session, global, id = "da")
  
  # **** UI ====
  # Update widgets when switching experiments and when performing the analysis ====
  observeEvent(c(global$metadataMatrix, global$daFinalSettings, global$daDoeTrigger), {
    if (is.null(global$daDoeResults)) return (NULL)
    
    # Get correct metadata subset and variables
    metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = global$daDoeResults$observationsSelected)
    variables <- biomex.getMetadataNames(metadataMatrix = metadataMatrix, ignoreFirstColumn = TRUE)  
    
    # Method settings
    updateSelectInput(session, "daDataMatrixAsInput", selected = global$daFinalSettings$dataMatrixAsInput)
    updateSelectizeInput(session, "daCovariates", choices = variables, selected = global$daFinalSettings$covariates)
    
    # The differential analysis method changes depending on the type of data that is being analyzed
    if (global$experimentInformation$Technology == "scRNA-seq") choices <- c("Limma", "MAST")
    else choices <- "Limma"
    updateSelectInput(session, "daType", choices = choices, selected = global$daFinalSettings$type)
  })
  
  # Group selection ====
  # Update selectInput based on the metadata columns
  observeEvent(c(global$metadataMatrix, global$daDoeResults, global$daDoeTrigger), {
    if (is.null(global$daDoeResults)) return (NULL)

    # Get the subset data matrix
    metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = global$daDoeResults$observationsSelected)

    # Update observationVariable
    choices <- biomex.getMetadataNames(metadataMatrix = metadataMatrix)
    selected <- biomex.getValidChoice(choices = choices, selected = global$daFinalSettings$observationVariable)
    updateSelectInput(session, "daObservationVariable", choices = choices, selected = selected)
    global$daSettings$observationVariable <- selected # Force update because if observationVariable doesn't get invalidated, this element will never be updated

    # Also update referenceGroup and experimentalGroup when the metadata is changed
    if (selected == "") return (NULL) # HACK: FIX: CRASH: If observationVariable was not set, then stop here
    choices <- unique(metadataMatrix[[global$daSettings$observationVariable]])

    # Reference group
    selected <- biomex.getValidChoice(choices = choices, selected = global$daFinalSettings$referenceGroup, defaultAsEmptyString = TRUE)
    updateSelectizeInput(session, "daReferenceGroup", choices = choices, selected = selected, server = TRUE)
    global$daSettings$referenceGroup <- selected # Force update because if observationGroup doesn't get invalidated, this element will never be updated

    # Experimental group
    selected <- biomex.getValidChoice(choices = choices, selected = global$daFinalSettings$experimentalGroup, defaultAsEmptyString = TRUE)
    updateSelectizeInput(session, "daExperimentalGroup", choices = choices, selected = selected, server = TRUE)
    global$daSettings$experimentalGroup <- selected # Force update because if observationGroup doesn't get invalidated, this element will never be updated
  })

  # Update the second selectInput based and the selection of observationVariable
  observeEvent(input$daObservationVariable, {
    if (is.null(global$metadataMatrix) || is.null(global$daDoeResults)) return (NULL)

    global$daSettings$observationVariable <- input$daObservationVariable

    # Get the subset data matrix
    metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = global$daDoeResults$observationsSelected)

    # Get the choices
    choices <- unique(metadataMatrix[[global$daSettings$observationVariable]])
    
    # When you are switching experiment, you don't want to reset referenceGroup and experimentalGroup, you want to display what it was
    if (global$switchingExperiment == TRUE || global$loadStoredResults == TRUE)
    {
      updateSelectizeInput(session, "daReferenceGroup", choices = choices, selected = global$daFinalSettings$referenceGroup, server = TRUE)
      updateSelectizeInput(session, "daExperimentalGroup", choices = choices, selected = global$daFinalSettings$experimentalGroup, server = TRUE)
      return (NULL) # Don't do anything after this
    }

    updateSelectizeInput(session, "daReferenceGroup", choices = choices, selected = "", server = TRUE)
    updateSelectizeInput(session, "daExperimentalGroup", choices = choices, selected = "", server = TRUE)
  })
  
  # Create mixed model table ====
  observeEvent(global$daSettings$covariates, {
    # If no covariates are present, do nothing
    if (is.null(global$daSettings$covariates) || global$daSettings$covariates == "") 
    {
      global$mixedModelTable <- NULL
      return (NULL)
    }
    
    # When switching experiment, don't perform this (we want to show the saved table in that case, not remake it)
    if (global$switchingExperiment == FALSE && global$loadStoredResults == FALSE)
    {
      covariates <- global$daSettings$covariates
      mixedModelTable <- matrix("", ncol = 4, nrow = length(covariates))
      mixedModelTable[, 1] <- covariates
      colnames(mixedModelTable) <- c("Variable", "Type", "Group", "Order")
      mixedModelTable <- as.data.table(mixedModelTable)
      global$mixedModelTable <- mixedModelTable
    }
  })
  
  # Update mixed model table ====
  observeEvent(input$mixedModelTable, {
    df <- as.data.table(hot_to_r(input$mixedModelTable))
    df[, Variable := global$mixedModelTable$Variable]
    setcolorder(df, unique(c("Variable", colnames(df)))) # Reorder so Variable is the first column
    global$mixedModelTable <- df
  })
  
  # **** Assign to global variables ====
  # Method settings
  observeEvent(input$daDataMatrixAsInput, {
    global$daSettings$dataMatrixAsInput <- input$daDataMatrixAsInput
  })
  
  observeEvent(input$daType, {
    global$daSettings$type <- input$daType
  })
  
  observeEvent(input$daCovariates, {
    global$daSettings$covariates <- biomex.validateSelectizeInputMultiple(value = input$daCovariates)
  }, ignoreNULL = FALSE)
  
  observeEvent(input$daObservationVariable, {
    global$daSettings$observationVariable <- biomex.validateSelectizeInputMultiple(value = input$daObservationVariable)
  })
  
  observeEvent(input$daReferenceGroup, {
    global$daSettings$referenceGroup <- biomex.validateSelectizeInputMultiple(value = input$daReferenceGroup)
  }, ignoreNULL = FALSE)
  
  observeEvent(input$daExperimentalGroup, {
    global$daSettings$experimentalGroup <- biomex.validateSelectizeInputMultiple(value = input$daExperimentalGroup)
  }, ignoreNULL = FALSE)

  # **** Perform differential analysis ====
  observeEvent(input$updateDifferentialAnalysis, {
    # If dataMatrixNormalized is NULL, do nothing
    if (is.null(global$dataMatrixNormalized)) return (NULL)
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing differential analysis...", style = global$progressStyle)
    on.exit(progress$close())
        
    # Call garbage collector
    biomex.callGC()
    
    # Get the right data matrix
    progress$set(message = "Performing differential analysis...", detail = "Preparing data", value = 1)
    
    # Obtain correct matrix
    if (global$daSettings$dataMatrixAsInput == "Normal" && global$experimentInformation$Technology == "RNA-seq" && global$experimentInformation$Data == "Raw")
    {
      observations <- biomex.getObservationNames(dataMatrix = global$dataMatrixNormalized)
      features <- biomex.getFeatureNames(dataMatrix = global$dataMatrixNormalized)
      dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = global$dataMatrixOriginal, rows = features, columns = observations)
      dataMatrix <- biomex.decompressMatrix(dataMatrix = dataMatrix)
    }
    else if (global$daSettings$dataMatrixAsInput == "Normal") dataMatrix <- global$dataMatrixNormalized
    else if (global$daSettings$dataMatrixAsInput == "Engineered") dataMatrix <- global$feResults$dataMatrixEngineered

    # Perform differential analysis
    progress$set(message = "Performing differential analysis...", detail = "Computing", value = 1)
    global$daResults <- biomex.differentialAnalysis(dataMatrix = dataMatrix, metadataMatrix = global$metadataMatrix, experimentInformation = global$experimentInformation, 
      mappingTable = global$mappingTable, dataMatrixAsInput = global$daSettings$dataMatrixAsInput, features = global$daDoeResults$featuresSelected, observations = global$daDoeResults$observationsSelected,
      variable = global$daSettings$observationVariable, referenceGroup = global$daSettings$referenceGroup,
      experimentalGroup = global$daSettings$experimentalGroup, covariates = global$daSettings$covariates, type = global$daSettings$type,
      mixedModelTable = global$mixedModelTable, cores = global$cores)
    
    # If it fails, stop and reset results
    if (!is.null(global$daResults$errorMessage)) 
    {
      js$alertMessage(global$daResults$errorMessage)
      global$daResults <- NULL
      global$daPlot <- NULL
      return (NULL)
    }
    
    # Remove unwanted variables
    biomex.removeVariables(variables = "dataMatrix")
    
    # Save settings
    global$daFinalSettings <- global$daSettings

    # # Reset downstream analyses (CSEA, SSEA and pathway mapping), but do not reset the stored results
    # storedResults <- c("cseaStoredResults", "sseaStoredResults", "pmStoredResults")
    # variablesToReset <- c(globalCompetitiveSetEnrichmentAnalysisVariables, globalCompetitiveSetEnrichmentAnalysisNotSaved,
    #   globalSelfContainedSetEnrichmentAnalysisVariables, globalSelfContainedSetEnrichmentAnalysisNotSaved,
    #   globalPathwayMappingVariables, globalPathwayMappingNotSaved)
    # variablesToReset <- setdiff(variablesToReset, storedResults)
    # 
    # biomex.resetGlobalVariables(global = global, original = globalOriginal, variables = variablesToReset)
        
    # Call garbage collector
    biomex.callGC()
  })
  
  # **** Save results for the meta analysis ====
  observeEvent(input$daMetaAnalysisSave, {
    name <- input$daMetaAnalysisName
    resultNames <- names(global$metaAnalysisData)
    table <- global$daResults$differentialExpression
    
    # You cannot save the result if there are no results
    if (is.null(table))
    {
      js$alertMessage("No results available to save.")
      return (NULL)
    }
    
    # If the name is empty, do nothing
    if (name == "")
    {
      js$alertMessage("You need to specify a name.")
      return (NULL)
    }
    
    # If the name is already present, do nothing
    if (name %in% resultNames)
    {
      js$alertMessage("This name already exists.")
      return (NULL)
    }
    
    progress <- biomex.createProgressObject(message = "Saving result...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Save differential analysis for the meta analysis
    maLength <- length(global$metaAnalysisData)
    if (maLength == 0) maLength <- 1
    else maLength <- maLength + 1
    
    global$metaAnalysisData[[maLength]] <- list(Name = name, Experiment = global$selectedExperiment, Variable = global$daFinalSettings$observationVariable,
      Feature = global$daDoeFinalSettings$featureSelected, Reference = global$daSettings$referenceGroup, Experimental = global$daSettings$experimentalGroup, Covariate = global$daSettings$covariates,
      Analysis = "Differential analysis", Matrix = global$daFinalSettings$dataMatrixAsInput, Comparison = global$daResults$differentialExpression)
    
    # Reset text widget
    updateTextInput(session, "daMetaAnalysisName", value = "")
    
    # Close modal
    toggleModal(session, "daMetaAnalysisSettingsModal", toggle = "close")
  })
  
  # **** Plots and tables ====
  # Differential expression table ====
  output$differentialAnalysisTable <- shiny::renderDataTable({
    # If there are no results, don't do anything
    if (is.null(global$daResults)) return (biomex.blankTable(text = "No results available."))
    
    differentialExpression <- global$daResults$differentialExpression
    
    if (input$daFeaturesToShow == "Significant (p-value)") differentialExpression <- differentialExpression[`Pvalue` < 0.05]
    if (input$daFeaturesToShow == "Significant (adjusted p-value)") differentialExpression <- differentialExpression[`Adjusted pvalue` < 0.05]
      
    differentialExpression
  }, options = list(scrollX = TRUE, autoWidth = FALSE))
  
  # Plot ====
  output$volcanoPlot <- renderPlotly({
    # If there are no results, don't do anything
    if (is.null(global$daResults)) 
    {
      global$daPlot <- NULL
      return (biomex.blankPlot(text = "No results available."))
    }
    
    # Get results
    differentialExpression <- global$daResults$differentialExpression
    
    # Get parameters
    useAdjustedPvalue <- input$daVolcanoUseAdjustedPvalue
    dotSize <- input$daVolcanoSize
    significantColor <- input$daSignificantColor
    notSignificantColor <- input$daNotSignificantColor
    
    # Define labels
    xLabel <- "Log2 (fold change)"
    yLabel <- "-Log10 (p-value)"
    
    # Get correct pvalue
    if (input$daVolcanoUseAdjustedPvalue == FALSE) 
    {
      pvalueColumn <- "Pvalue"
      yLabel <- "-Log10 (p-value)"
    }
    if (input$daVolcanoUseAdjustedPvalue == TRUE) 
    {
      pvalueColumn <- "Adjusted pvalue"
      yLabel <- "-Log10 (adjusted p-value)"
    }
      
    significantThreshold <- 0.05
    significance <- differentialExpression[[pvalueColumn]] < significantThreshold
      
    # Define axis
    ax <- list(zeroline = FALSE, showline = TRUE, mirror = "ticks", gridcolor = toRGB("gray50"), gridwidth = 1, zerolinecolor = toRGB("black"),
            zerolinewidth = 1, linecolor = toRGB("black"), linewidth = 5, titlefont = list(size = 15))
    
    # Get data to plot
    logFC <- differentialExpression[["Log fold change"]]
    logPvalue <- differentialExpression[[pvalueColumn]]
    logPvalue[logPvalue == 0] <- min(logPvalue[logPvalue != 0]) # If the Pvalue is 0, set it to the next minimum
    logPvalue <- -log10(logPvalue)
    features <- differentialExpression$Feature
    
    # Set the right colors
    if (length(unique(significance)) == 1 && unique(significance) == TRUE) colors <- significantColor
    else if (length(unique(significance)) == 1 && unique(significance) == FALSE) colors <- notSignificantColor
    else colors <- c(notSignificantColor, significantColor)
    
    # Check colors validity
    if (biomex.areColors(colors = colors) == FALSE) 
    {
      global$daPlot <- NULL
      return (biomex.blankPlot(text = "Invalid color detected."))
    }
    
    p <- plot_ly(x = logFC, y = logPvalue, color = significance, colors = colors, marker = list(size = dotSize), type = "scatter", mode = "markers",
      text = features)  %>% layout(showlegend = FALSE, xaxis = c(ax, title = xLabel), yaxis = c(ax, title = yLabel))
      
    global$daPlot <- p
    
    if (global$doNotUseWebGL == TRUE) p else toWebGL(p)
  })
  
  # Mixed model table ====
  output$mixedModelTable <- renderRHandsontable({
    if (!is.null(global$mixedModelTable)) 
    {
      type <- c("Fixed", "Random")
      df <- global$mixedModelTable
    
      rhandsontable(df[, -"Variable"], rowHeaders = df[, Variable], stretchH = "all", rowHeaderWidth = 250, 
                    fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
        hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
        hot_cols(columnSorting = FALSE, manualColumnResize = FALSE, halign = "htCenter") %>%
        hot_col(col = "Type",  type = "dropdown", source = type, strict = TRUE, allowInvalid = FALSE) %>%
        hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
    }
  })
    
  # *** Export ====
  # Plot export ====
  observeEvent(input$exportDaPlot, {
    if (is.null(global$daPlot))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportPlot <- global$daPlot
    
    # Open modal
    toggleModal(session, "exportPlotModal", toggle = "open")
  })
  
  # Table export ====
  observeEvent(input$exportDaTable, {
    if (is.null(global$daResults))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
        
    differentialExpression <- global$daResults$differentialExpression
    
    if (input$daFeaturesToShow == "Significant (p-value)") differentialExpression <- differentialExpression[`Pvalue` < 0.05]
    if (input$daFeaturesToShow == "Significant (adjusted p-value)") differentialExpression <- differentialExpression[`Adjusted pvalue` < 0.05]
      
    global$exportTable <- differentialExpression
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
}