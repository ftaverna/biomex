#' The logic of the Design of Experiment tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicBruteForce <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "bf", allowDoE = FALSE)
  
  # **** Store and load results ====
  biomex.storeAndLoadResultsLogic(input, output, session, global, id = "bf")
  
  # **** Feature selection ====
  biomex.featureSelectionLogic(input, output, session, global, id = "bfShow")
  
  # **** UI ====
  # Update widgets when switching experiments and when performing the analysis ====
  observeEvent(c(global$bfFinalSettings, global$bfDoeTrigger), {
    # Method settings
    updateSelectInput(session, "bfAnalysis", selected = global$bfFinalSettings$analysis)
    updateSelectInput(session, "bfDrColorCoding", selected = global$bfFinalSettings$drColorCoding)
    updateSliderInput(session, "bfDrDotSize", value = global$bfFinalSettings$drDotSize)
    updateCheckboxInput(session, "bfTestRun", value = global$bfFinalSettings$testRun)
  })
  
  # **** Assign to global variables ====
  # Method settings
  observeEvent(input$bfAnalysis, {
    global$bfSettings$analysis <- input$bfAnalysis
  })
  
  observeEvent(input$bfDrColorCoding, {
    global$bfSettings$drColorCoding <- input$bfDrColorCoding
  })
  
  observeEvent(input$bfDrDotSize, {
    global$bfSettings$drDotSize <- input$bfDrDotSize
  })
  
  observeEvent(input$bfTestRun, {
    global$bfSettings$testRun <- input$bfTestRun
  })
  
  # **** Click on plot ====
  observeEvent(input$clickBfPlot, {
    toDisplay <- input$clickBfPlot
    index <- which(global$bfAdditionalResults$featureSelected == toDisplay)
    
    if (length(index) == 0) global$bfAdditionalResults$featureSelected <- c(global$bfAdditionalResults$featureSelected, toDisplay)
    else global$bfAdditionalResults$featureSelected <- global$bfAdditionalResults$featureSelected[-index]
    
    js$resetWidget("clickBfPlot")
  })
  
  # **** Select feature to show ====
  observeEvent(input$updateBruteforceFeaturesSelected, {
    if (is.null(global$bfResults))
    {
      js$alertMessage("You need to compute the brute force analysis first.")
      return (NULL)
    }
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing feature selection...", style = global$progressStyle)
    on.exit(progress$close())
            
    # Call garbage collector
    biomex.callGC()
    
    # Get the right data matrix
    progress$set(message = "Performing feature selection...", detail = "Preparing data", value = 1)
    
    # Get important variables
    mappingTable <- global$mappingTable
        
    # Create a fake score matrix (it can contain only the observations)
    scoresMatrix <- data.table(Observation = global$bfResults$observations)
      
    bfDataResults <- biomex.obtainDataForBruteForce(dataMatrixNormalized = global$dataMatrixNormalized, dataMatrixEngineered = global$feResults$dataMatrixEngineered, 
      type = global$bfFinalSettings$analysis, mappingTable = global$mappingTable,
      drDataMatrixAsInput = global$drFinalSettings$dataMatrixAsInput, drDataMatrixNormalizedGrouped = global$drDoeResults$dataMatrixNormalizedGrouped, drDataMatrixEngineeredGrouped = global$drDoeResults$dataMatrixEngineeredGrouped,
      drColorCoding = global$bfFinalSettings$drColorCoding, drScoresMatrix = scoresMatrix,
      testRun = global$bfFinalSettings$testRun, converFeatures = FALSE)

    if (!is.null(bfDataResults$errorMessage))
    {
      js$alertMessage(bfDataResults$errorMessage)
      return (NULL)
    }

    # Perform feature selection
    progress$set(message = "Performing feature selection...", detail = "Computing", value = 1)
    globalSettingsId <- "bfShowSettings"
    
    # If the matrix is the engineered one, then we need to set the mapping table to NULL, since the engineered matrix doesn't have a mapping table
    if (global$bfFinalSettings$drColorCoding == "Engineered feature") mappingTable <- NULL

    resultList <- biomex.getFeatureSubset(dataMatrix = bfDataResults$dataMatrix, mappingTable = mappingTable,
      experimentInformation = global$experimentInformation, observationsSelected = bfDataResults$scoresMatrix$Observation,
      featureSelected = global[[globalSettingsId]]$featureSelected, featureSelectedCustomPath = global[[globalSettingsId]]$featureSelectedCustomPath,
      hvMeanLowThreshold = global[[globalSettingsId]]$hvMeanLowThreshold, hvMeanHighThreshold = global[[globalSettingsId]]$hvMeanHighThreshold, 
      hvDispersionThreshold = global[[globalSettingsId]]$hvDispersionThreshold, hvNumberOfBins = global[[globalSettingsId]]$hvNumberOfBins,
      hvPerformOnAllObservations = global[[globalSettingsId]]$hvPerformOnAllObservations, convertTo = "Name")


    if (!is.null(resultList$errorMessage))
    {
      js$alertMessage(resultList$errorMessage)
      return (NULL)
    }
        
    global$bfAdditionalResults$featuresToShow <- resultList$featuresSelected
    
    # Remove unwanted variables
    biomex.removeVariables(variables = c("bfDataResults", "resultList"))
    
    # Close modal
    toggleModal(session, "bfFeaturesSettingsModal", toggle = "close")
    
    # Call garbage collector
    biomex.callGC()
  })
  
  # **** Perform brute force ====
  observeEvent(input$updateBruteForce, {
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing brute force...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Call garbage collector
    biomex.callGC()
    
    # Get the right data matrix
    progress$set(message = "Performing brute force...", detail = "Preparing data", value = 1)
    bfDataResults <- biomex.obtainDataForBruteForce(dataMatrixNormalized = global$dataMatrixNormalized, dataMatrixEngineered = global$feResults$dataMatrixEngineered, 
      type = global$bfSettings$analysis, mappingTable = global$mappingTable,
      drDataMatrixAsInput = global$drFinalSettings$dataMatrixAsInput, drDataMatrixNormalizedGrouped = global$drDoeResults$dataMatrixNormalizedGrouped, drDataMatrixEngineeredGrouped = global$drDoeResults$dataMatrixEngineeredGrouped,
      drColorCoding = global$bfSettings$drColorCoding, drScoresMatrix = global$drResults$scoresMatrix,
      testRun = global$bfSettings$testRun, converFeatures = TRUE)
    
    if (!is.null(bfDataResults$errorMessage))
    {
      js$alertMessage(bfDataResults$errorMessage)
      return (NULL)
    }
    
    # Perform brute force
    progress$set(message = "Performing brute force...", detail = "Generating plots", value = 1)
    bfResults <- biomex.performBruteForce(dataResults = bfDataResults, mappingTable = global$mappingTable, type = global$bfSettings$analysis, drDotSize = global$bfSettings$drDotSize)
    
    if (is.null(bfResults))
    {
      js$alertMessage("Brute force plotting failed.")
      return (NULL)
    }
    
    global$bfResults <- bfResults
    
    # Remove unwanted variables
    biomex.removeVariables(variables = c("bfDataResults", "bfResults"))
    
    # Reset results
    global$bfAdditionalResults <- NULL
    global$bfAdditionalResults$dummyResult <- "" # Need to put something in the results variable
    global$bfAdditionalResults$featuresToShow <- global$bfResults$features
    
    # Save settings
    global$bfFinalSettings <- global$bfSettings
        
    # Call garbage collector
    biomex.callGC()
  })
  
  # *** Plots and tables ====
  # Brute force table with plots ====
  output$bfTablePlot <- DT::renderDataTable({
    input$updateBruteForce # FIX: Needs to be updated on click also, otherwise if you regenerate the plots with the same filenames, it will not updated
    input$updateBruteforceFeaturesSelected # Update when the features selected change
    
    if (is.null(global$bfResults)) return (DT::datatable(biomex.blankTable(text = "No results available."), selection = "none", rownames = FALSE))

    # Parameters
    columns <- as.integer(biomex.validateNumericInput(value = input$bfColumns, defaultTo = 1, min = 1, max = 100))
    rows <- as.integer(biomex.validateNumericInput(value = input$bfRows, defaultTo = 5, min = 1))
    size <- input$bfSize
    
    # Create data.table
    fileTable <- global$bfResults$fileTable
    featuresToShow <- isolate(global$bfAdditionalResults$featuresToShow) # Isolate to avoid unwanted updates when the user click on a picture
    
    # Subset to select the features to show
    if (!is.null(featuresToShow)) fileTable <- fileTable[Feature %in% featuresToShow]
    n <- nrow(fileTable)
    
    if (nrow(fileTable) == 0) return (DT::datatable(biomex.blankTable(text = "No features selected."), selection = "none", rownames = FALSE))
    
    createImages <- function(x, featureSelected, size, time)
    {
      feature <- x["Feature"]
      file <- x["File"]
      toDisplay <- x["ToDisplay"]
      src <- x["Base64"]
      
      opacity <- if (toDisplay %in% featureSelected) 0.4 else 1
      paste0('<img src = "', src, '" width = "', size, '" height = "', size, '" class = "clickBfPlot" style = "opacity: ', opacity, '" data-value = "', toDisplay, '"></img>')
      # PERFORMANCE: FIX: do not use tags$img and then as.character, it takes too much time
      # as.character(tags$img(src = src, width = size, height = size, class = "clickBfPlot", style = paste0("opacity: ", opacity), 'data-value' = toDisplay, time = time))
    }
    
    # bfAdditionalResults must be isolated to prevent unwanted updating when clicking
    images <- apply(fileTable, 1, createImages, featureSelected = isolate(global$bfAdditionalResults$featureSelected), size = size, time = as.numeric(Sys.time()))
    images <- unlist(images, use.names = FALSE, recursive = FALSE)
    
    # Add what is missing
    missing <- columns - (n %% columns)
    if (missing == columns) missing <- 0
    images <- c(images, rep("", missing))
    
    # Generate table
    table <- as.data.table(matrix(images, byrow = TRUE, ncol = columns))
    colnames(table) <- rep(" ", ncol(table))
    
    # Cannot handle the pages inside DT because it messes the updating. So the pages are selected in the menu, not in the DT directly.
    DT::datatable(table, escape = FALSE, selection = "none", rownames = FALSE, options = list(scrollX = TRUE, pageLength = rows, lengthChange = FALSE, searching = FALSE, ordering = FALSE, info = FALSE, autoWidth = FALSE))
  })
   
   # Show selected table ====
   output$bfShowSelectedTable <- shiny::renderDataTable({
     if (is.null(global$bfAdditionalResults$featureSelected)) return (biomex.blankTable(text = "No elements selected."))
     
     data.table(Selected = global$bfAdditionalResults$featureSelected)
   }, options = list(scrollX = TRUE, autoWidth = FALSE))
}