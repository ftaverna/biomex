#' The logic of the metadata tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicMetadata <- function(input, output, session, global)
{
  # *** UI ====
  # We don't allow to add and modify the metadata with scRNA-seq, because there are too many observations
  # observeEvent(global$experimentInformation, {
  #   shinyjs::show("addMetadata")
  #   shinyjs::show("modifyMetadata")
  #   
  #   if (global$experimentInformation$Technology == "scRNA-seq")
  #   {
  #     shinyjs::hide("addMetadata")
  #     shinyjs::hide("modifyMetadata")
  #   }
  # })
  
  # Show/hide based on whether BIOMEX is online or offline
  observeEvent(global$online, {
    shinyjs::hide("addByFileMetadataSelect")
    shinyjs::hide("addMetadataFilePath")
    shinyjs::hide("addByFileMetadataSelectOnline")
    
    if (global$online == TRUE) shinyjs::show("addByFileMetadataSelectOnline")
    if (global$online == FALSE)
    {
      shinyjs::show("addByFileMetadataSelect")
      shinyjs::show("addMetadataFilePath")
    }
  })
  
  # *** Metadata table ====
  output$metadataMatrix <- shiny::renderDataTable({
    if (input$metadataType == "Full") 
    {
      metadataMatrix <- global$metadataMatrix
    }
    
    if (input$metadataType == "After pretreatment") 
    {
      if (is.null(global$dataMatrixNormalized)) return (data.table(Information = "Pretreatment not performed yet."))
      metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = biomex.getObservationNames(dataMatrix = global$dataMatrixNormalized))
    }
    
    if (input$metadataType == "Subset") 
    {
      resultList <- biomex.getDataInformationSubset(global = global, subsetType = input$metadataSubsetType)
      
      if (is.null(resultList$observationsSelected)) return (data.table(Information = "Subset not performed yet."))
      metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = resultList$observationsSelected)
    }
    
    as.data.frame(metadataMatrix)
  }, options = list(scrollX = TRUE, autoWidth = FALSE))
  
  # *** Add metadata ====
  # Update UI ====
  # Nothing to update
  
  # Rhandsontable ====
  output$addMetadataMatrix <- renderRHandsontable({
    df <- global$addMetadataMatrix
        
    # If there are too many rows, don't create the table
    if (is.null(df)) return (biomex.blankHandsontable(text = "Too many factors to create the table.", header = "Warning"))
    
    rhandsontable(df[, -"Observation"], rowHeaders = df[, Observation], stretchH = "all", rowHeaderWidth = 250, 
                  fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
      hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
      hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
  })
  
  observeEvent(input$addMetadataMatrix, {
    if (is.null(global$addMetadataMatrix)) return (NULL)
    
    observations <- global$metadataMatrix$Observation
    global$addMetadataMatrix <- data.table(cbind(Observation = observations, hot_to_r(input$addMetadataMatrix)))
  })
  
  # Open modal ====
  observeEvent(input$addMetadata, {
    # Create and empty table and display it
    global$addMetadataMatrix <- data.table(Observation = global$metadataMatrix$Observation, Value = "")
    
    # If there are too many rows, put the table to NULL
    if (nrow(global$addMetadataMatrix) > global$tableLimit) global$addMetadataMatrix <- NULL
    
    # Clear text input and open modal
    updateTextInput(session, "addMetadataName", value = "")
    toggleModal(session, "addMetadataModal", toggle = "open")
  })
  
  # Handle the adding ====
  observeEvent(input$confirmAddMetadata, {
    if (is.null(global$addMetadataMatrix))
    {
      js$alertMessage("The groups must be defined first.")
      return (NULL)
    }
    
    # If the name is empty, give a warning
    if (input$addMetadataName == "")
    {
      js$alertMessage("You need to insert a name.")
      return (NULL)
    }
    
    # Make the chosen name unique
    columns <- colnames(global$metadataMatrix)
    name <- rev(make.unique(c(columns, input$addMetadataName)))[1]
    
    # Get values
    metadataToAdd <- global$addMetadataMatrix$Value
    
    # Add column
    global$metadataMatrix[[name]] <- metadataToAdd 
    # metadataMatrix[, eval(name) := metadataToAdd] # Shiny reactivity doesn't work when updating by reference
    global$addMetadataMatrix <- NULL
    
    # Close modal
    toggleModal(session, "addMetadataModal", toggle = "close")
  })
  
  # *** Add metadata by file ====
  # Offline file select ====
  observeEvent(input$addByFileMetadataSelect, {
    # Select file
    filePath <- biomex.chooseFile()
    if (is.null(filePath)) return (NULL)
    
    # Read file
    additionalMetadataMatrix <- biomex.readFile(filePath = filePath)
    
    global$addMetadataFilePath <- filePath
    global$additionalMetadataMatrix <- additionalMetadataMatrix
  })
  
  # Online file select ====
  observeEvent(input$addByFileMetadataSelectOnline, {
    # Read file
    additionalMetadataMatrix <- biomex.readFile(filePath = input$addByFileMetadataSelectOnline$datapath)
    
    global$additionalMetadataMatrix <- additionalMetadataMatrix
  })
  
  # Handle the adding ====
  observeEvent(input$confirmAddByFileMetadata, {
    if (is.null(global$additionalMetadataMatrix))
    {
      js$alertMessage("You need to select a file first.")
      return (NULL)
    }
    
    metadataMatrix <- global$metadataMatrix
    additionalMetadataMatrix <- global$additionalMetadataMatrix
    
    # Change column names (and make them unique)
    columnNames <- colnames(additionalMetadataMatrix)
    metadataColumnNames <- colnames(metadataMatrix)
    uniqueNames <- make.unique(c(metadataColumnNames, columnNames))
    columnNames <- uniqueNames[(length(metadataColumnNames) + 1):(length(metadataColumnNames) + length(columnNames))]
    columnNames[1] <- "Observation"
    setnames(additionalMetadataMatrix, colnames(additionalMetadataMatrix), columnNames)
    
    # Trim all whitespaces from the metadata matrix
    notNumericColumnsIndex <- which(sapply(additionalMetadataMatrix, class) != "numeric")
    if (length(notNumericColumnsIndex) > 0)
    {
      notNumericColumns <- colnames(additionalMetadataMatrix)[notNumericColumnsIndex]
      for (column in notNumericColumns) additionalMetadataMatrix[, (column) := trimws(additionalMetadataMatrix[[column]])]
    }
    
    # Merge the metadata files
    previousOrdering <- metadataMatrix$Observation
    previousColumnNames <- colnames(metadataMatrix)
    lastColumn <- ncol(metadataMatrix)
    metadataMatrix <- merge(metadataMatrix, additionalMetadataMatrix, by = "Observation", all.x = TRUE, sort = FALSE)
    metadataMatrix[is.na(metadataMatrix)] <- ""
    
    # Restore previous column names
    setnames(metadataMatrix, 1:lastColumn, previousColumnNames)
    
    # Reset upload button
    shinyjs::reset("addByFileMetadataSelectOnline")
    
    global$additionalMetadataMatrix <- NULL
    global$addMetadataFilePath <- NULL
    global$metadataMatrix <- metadataMatrix
    
    # Close modal
    toggleModal(session, "addByFileMetadataModal", toggle = "close")
  })
  
  # Render path ====
  output$addMetadataFilePath <- shiny::renderDataTable({
    if (is.null(global$addMetadataFilePath)) return (biomex.blankTable(text = "No file selected."))

    data.table(Path = global$addMetadataFilePath)
  }, options = list(scrollX = TRUE, paging = FALSE, searching = FALSE, info = FALSE, autoWidth = FALSE), callback = JS('function(settings) { $("#addMetadataFilePath thead").remove(); }'))
  
  
  # *** Copy metadata ====
  # Update UI ====
  observeEvent(global$metadataMatrix, {
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    # selected <- biomex.getValidChoice(choices = choices, selected = input$copyColumnMetadataChoice)
    selected <- choices[1]
    
    updateSelectInput(session, "copyColumnMetadataChoice", choices = choices, selected = selected)
  })
  
  # Open modal ====
  observeEvent(input$copyColumnMetadata, {
    # Check if there is metadata. If there is not, don't open the modal.
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    updateTextInput(session, "copyColumnMetadataName", value = "")
    updateSelectInput(session, "copyColumnMetadataChoice", choices = choices, selected = choices[1])
    
    # Create and empty table and display it
    toggleModal(session, "copyColumnMetadataModal", toggle = "open")
  })
  
  # Handle the copying ====
  observeEvent(input$confirmCopyColumnMetadata, {
    name <- input$copyColumnMetadataName
    choice <- input$copyColumnMetadataChoice
    
    if (any(name %in% "") || is.null(choice))
    {
      js$alertMessage("You need to fill out both the name and the column to copy.")
      return (NULL)
    }
    
    if (name %in% colnames(global$metadataMatrix))
    {
      js$alertMessage("The new column name is already in use. Select another one.")
      return (NULL)
    }
    
    # Copy the metadata
    newColumn <- global$metadataMatrix[[choice]]
    global$metadataMatrix[[name]] <- newColumn # HACK: Have to do this, since it doesn't work by reference with global
    
    # Copy the metadata colors (if necessary)
    if (!is.null(global$metadataColors[[name]])) global$metadataColors[[name]] <- global$metadataColors[[choice]]
    
    # Close modal
    toggleModal(session, "copyColumnMetadataModal", toggle = "close")
  })
  
  # *** Modify metadata ====
  # Update UI ====
  observeEvent(global$metadataMatrix, {
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    selected <- biomex.getValidChoice(choices = choices, selected = input$modifyMetadataName)
    
    updateSelectInput(session, "modifyMetadataName", choices = choices, selected = selected)
  })
  
  # Rhandsontable ====
  output$modifyMetadataMatrix <- renderRHandsontable({
    df <- global$modifyMetadataMatrix
        
    # If there are too many rows, don't create the table
    if (is.null(df)) return (biomex.blankHandsontable(text = "Too many factors to create the table.", header = "Warning"))
    
    rhandsontable(df[, -"Observation"], rowHeaders = df[, Observation], stretchH = "all", rowHeaderWidth = 250, 
                  fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
      hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
      hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
  })
  
  observeEvent(input$modifyMetadataMatrix, {
    if (is.null(global$modifyMetadataMatrix)) return (NULL)
    
    observations <- global$metadataMatrix$Observation
    global$modifyMetadataMatrix <- data.table(cbind(Observation = observations, hot_to_r(input$modifyMetadataMatrix)))
  })
  
  # Open modal ====
  observeEvent(input$modifyMetadata, {
    # Check if there is metadata. If there is not, don't open the modal.
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    # Create and empty table and display it
    toggleModal(session, "modifyMetadataModal", toggle = "open")
  })
  
  # Update table ====
  observeEvent(c(input$modifyMetadataName, input$modifyMetadata), { # Do it also when clicking the button, so it cleans the table
    name <- input$modifyMetadataName
    
    # Check if there is metadata. If there is not, do nothing
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    # No metadata columns, do nothing
    if(name == "") return (NULL)

    global$modifyMetadataMatrix <- data.table(Observation = global$metadataMatrix$Observation, Value = global$metadataMatrix[[name]])
    
    # If there are too many rows, put the table to NULL
    if (nrow(global$modifyMetadataMatrix) > global$tableLimit) global$modifyMetadataMatrix <- NULL
  }, ignoreInit = TRUE)
  
  # Handle the modification ====
  observeEvent(input$confirmModifyMetadata, {
    if (is.null(global$modifyMetadataMatrix))
    {
      js$alertMessage("The groups must be defined first.")
      return (NULL)
    }
    
    # Get name
    name <- input$modifyMetadataName
    
    # If the column is used in the design of experiment, avoid modifying it
    if (biomex.checkVariableInUse(variable = name, global = global))
    {
      js$alertMessage("The variable is in use and cannot be modified.")
      return (NULL)
    }
    
    # Get values
    metadataToAdd <- global$modifyMetadataMatrix$Value
    
    # Modify column
    global$metadataMatrix[[name]] <- metadataToAdd 
    # metadataMatrix[, eval(name) := metadataToAdd] # Shiny reactivity doesn't work when updating by reference
    global$modifyMetadataMatrix <- NULL
    
    # Update the metadata colors (if necessary)
    if (!is.null(global$metadataColors[[name]]))
    {
      # Find the present colors, the missing colors, and merge them together
      newValues <- unique(metadataToAdd)
      metadataColors <- global$metadataColors[[name]]
      index <- which(metadataColors$Value %in% newValues)
      presentColors <- metadataColors[index]
      index <- which(!newValues %in% metadataColors$Value)
      missingColors <- data.table(Value = newValues[index], Color = rep("black", length(index)))
      metadataColors <- rbind(presentColors, missingColors)
      
      # Order the metada colors by factor ordering
      ordering <- levels(factor(metadataColors$Value))
      metadataColors <- biomex.getDataTableSubset(dataTable = metadataColors, rows = ordering)
      global$metadataColors[[name]] <- metadataColors
    }
    
    # Close modal
    toggleModal(session, "modifyMetadataModal", toggle = "close")
  })
  
  # *** Merge metadata ====
  # Update UI ====
  observeEvent(global$metadataMatrix, {
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    selected <- biomex.getValidChoice(choices = choices, selected = input$mergeColumnMetadataChoice)
    
    updateSelectInput(session, "mergeColumnMetadataChoice", choices = choices, selected = selected)
  })
  
  # Open modal ====
  observeEvent(input$mergeColumnMetadata, {
    # Check if there is metadata. If there is not, don't open the modal.
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    updateTextInput(session, "mergeColumnMetadataName", value = "")
    updateSelectizeInput(session, "mergeColumnMetadataChoice", choices = choices, selected = "")
    
    # Create and empty table and display it
    toggleModal(session, "mergeColumnMetadataModal", toggle = "open")
  })
  
  # Handle the merging ====
  observeEvent(input$confirmMergeColumnMetadata, {
    name <- input$mergeColumnMetadataName
    choices <- input$mergeColumnMetadataChoice
    
    if (any(name %in% "") || is.null(choices))
    {
      js$alertMessage("You need to fill out both the name and the columns to merge.")
      return (NULL)
    }
    
    # Merge metadata
    newColumn <- apply(global$metadataMatrix[, choices, with = FALSE], 1, paste0, collapse = "_")
    global$metadataMatrix[[name]] <- newColumn # HACK: Have to do this, since it doesn't work by reference with global
    
    # Close modal
    toggleModal(session, "mergeColumnMetadataModal", toggle = "close")
  })
  
  # **** Change metadata values ====
  # Update UI ====
  observeEvent(global$metadataMatrix, {
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    selected <- biomex.getValidChoice(choices = choices, selected = input$changeValuesMetadataName)
    
    updateSelectInput(session, "changeValuesMetadataName", choices = choices, selected = selected)
  })
  
  observeEvent(c(input$changeValuesMetadataName, input$changeValuesMetadata), {
    if (input$changeValuesMetadataName == "") return (NULL)
    
    metadataMatrix <- global$metadataMatrix
    column <- input$changeValuesMetadataName
    choices <- unique(metadataMatrix[[column]])
    
    updateSelectizeInput(session, "changeValuesMetadataChoice", choices = choices, selected = "", server = TRUE)
  })
  
  # Open modal ====
  observeEvent(input$changeValuesMetadata, {
    # Check if there is metadata. If there is not, don't open the modal.
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    updateTextInput(session, "changeValuesMetadataValue", value = "")
    
    # Create and empty table and display it
    toggleModal(session, "changeValuesMetadataModal", toggle = "open")
  })
  
  # Handle the value changing ====
  observeEvent(input$confirmChangeValuesMetadata, {
    name <- input$changeValuesMetadataName
    choices <- input$changeValuesMetadataChoice
    newValue <- input$changeValuesMetadataValue
    
    if (any(name %in% "") || is.null(choices))
    {
      js$alertMessage("You need to fill out both the name and the values to change.")
      return (NULL)
    }
    
    # If the column is used in the design of experiment, avoid modifying it
    if (biomex.checkVariableInUse(variable = name, global = global))
    {
      js$alertMessage("The variable is in use and cannot be modified.")
      return (NULL)
    }
    
    # Change metadata
    metadataMatrix <- global$metadataMatrix
    subsetLogical <- metadataMatrix[[name]] %in% choices
    metadataMatrix[subsetLogical, name] <- newValue
    global$metadataMatrix <- metadataMatrix
    
    # Update the metadata colors (if necessary)
    if (!is.null(global$metadataColors[[name]]))
    {   
      # Based on the number of choices. With 1 choices we can just override the old one, with multiple choices we must delete the old values and add the new one.
      if (length(choices) == 1) 
      {
        metadataColors <- global$metadataColors[[name]]
        oldValues <- choices
        metadataColors[Value == oldValues, "Value"] <- newValue
      }
      else 
      {
        metadataColors <- global$metadataColors[[name]]
        oldValues <- choices
        index <- which(metadataColors$Value %in% oldValues)
        newColor <- metadataColors[index]$Color[1] # Take the first color
        metadataColors <- metadataColors[-index]
        newEntry <- data.table(Value = newValue, Color = newColor)
        metadataColors <- rbind(metadataColors, newEntry)
      }
      
      # Order the metada colors by factor ordering
      ordering <- levels(factor(metadataColors$Value))
      metadataColors <- biomex.getDataTableSubset(dataTable = metadataColors, rows = ordering)
      global$metadataColors[[name]] <- metadataColors
    }
    
    # Close modal
    toggleModal(session, "changeValuesMetadataModal", toggle = "close")
  })
  
  # *** Delete metadata ====
  # Update UI ====
  observeEvent(global$metadataMatrix, {
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    selected <- biomex.getValidChoice(choices = choices, selected = input$deleteMetadataName)
    
    updateSelectInput(session, "deleteMetadataName", choices = choices, selected = selected)
  })
  
  # Open modal ====
  observeEvent(input$deleteMetadata, {
    # Check if there is metadata. If there is not, don't open the modal.
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    # Create and empty table and display it
    toggleModal(session, "deleteMetadataModal", toggle = "open")
  })
  
  # Handle the modification ====
  observeEvent(input$confirmDeleteMetadata, {
    # Get name
    name <- input$deleteMetadataName
      
    # If nothing is selected, do nothing
    if (is.null(name) || name == "")
    {
      js$alertMessage("You need to select at least one variable.")
      return (NULL)
    }
    
    # If the column is used in the design of experiment, avoid deleting it
    if (biomex.checkVariableInUse(variable = name, global = global))
    {
      js$alertMessage("The variable is in use and cannot be deleted.")
      return (NULL)
    }
    
    # Delete column
    global$metadataMatrix[[name]] <- NULL 
    # metadataMatrix[, eval(name) := NULL] # Shiny reactivity doesn't work when updating by reference
        
    # If the metadata column is deleted, we also need to delete the color scheme
    global$metadataColors[[name]] <- NULL
    
    # Close modal
    toggleModal(session, "deleteMetadataModal", toggle = "close")
  })
  
  # *** Delete multiple metadata ====
  # Update UI ====
  observeEvent(global$metadataMatrix, {
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    selected <- ""
    
    updateSelectInput(session, "deleteMultipleMetadataName", choices = choices, selected = selected)
  })
  
  # Open modal ====
  observeEvent(input$deleteMultipleMetadata, {
    # Check if there is metadata. If there is not, don't open the modal.
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    # Create and empty table and display it
    toggleModal(session, "deleteMultipleMetadataModal", toggle = "open")
  })
  
  # Handle the modification ====
  observeEvent(input$confirmDeleteMultipleMetadata, {
    # Get name
    names <- input$deleteMultipleMetadataName
    
    # If nothing is selected, do nothing
    if (is.null(names) || names == "")
    {
      js$alertMessage("You need to select at least one variable.")
      return (NULL)
    }
    
    # If the column is used in the design of experiment, avoid deleting it
    for (name in names)
    {
      if (biomex.checkVariableInUse(variable = name, global = global))
      {
        js$alertMessage(paste("The variable", name, "is in use and cannot be deleted."))
        return (NULL)
      }
    }
    
    for (name in names) 
    {
      # Delete column
      global$metadataMatrix[[name]] <- NULL 
      # metadataMatrix[, eval(name) := NULL] # Shiny reactivity doesn't work when updating by reference
        
      # If the metadata column is deleted, we also need to delete the color scheme
      global$metadataColors[[name]] <- NULL
    }
    
    # Close modal
    toggleModal(session, "deleteMultipleMetadataModal", toggle = "close")
  })
  
  # *** Restore metadata ====
  # Handle the restoration ====
  observeEvent(input$restoreMetadata, {
    global$metadataMatrix <- global$metadataMatrixOriginal
        
    # If the metadata is reset, the color scheme must be reset too
    global$metadataColors <- NULL
  })
  
  # *** Define colors metadata ====
  # Update UI ====
  observeEvent(global$metadataMatrix, {
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    selected <- biomex.getValidChoice(choices = choices, selected = input$defineColorsMetadataName)
    
    updateSelectInput(session, "defineColorsMetadataName", choices = choices, selected = selected)
  })
  
  # Rhandsontable ====
  output$defineColorsMetadataMatrix <- renderRHandsontable({
    name <- input$defineColorsMetadataName
    df <- global$defineColorsMetadataMatrix
    
    if (name %in% global$reservedVariables) return (biomex.blankHandsontable(text = "Cannot define colors on reserved variables.", header = "Warning"))
    if (is.null(df)) return (biomex.blankHandsontable(text = "Too many factors to create the table.", header = "Warning"))
    
    rhandsontable(df[, -"Value"], rowHeaders = df[, Value], stretchH = "all", rowHeaderWidth = 250, 
                  fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
      hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
      hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
  })
  
  # Plot ====
  output$defineColorsMetadataPlot <- renderPlotly({
    if (is.null(global$defineColorsMetadataMatrix)) return (biomex.blankPlot(text = "No metadata selected."))
    
    # Get the values 
    values <- global$defineColorsMetadataMatrix$Value
    
    # Get the colors
    colors <- global$defineColorsMetadataMatrix$Color
    
    # Create some fake data to plot
    barplotData <- rep(1, length(values))
    
    # If the colors are not valid, do not plot
    if (biomex.areColors(colors = colors) == FALSE) return (biomex.blankPlot(text = "Invalid color detected."))
    
    p <- plot_ly(x = values, y = barplotData, color = values, colors = colors, type = "bar") %>%      
      layout(title = '', showlegend = FALSE,
      xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = TRUE, fixedrange = TRUE),
      yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE, fixedrange = TRUE)) %>% config(displayModeBar = F)
    
    if (global$doNotUseWebGL == TRUE) p else toWebGL(p)
  })
  
  observeEvent(input$defineColorsMetadataMatrix, {
    name <- input$defineColorsMetadataName
    
    # No metadata columns, do nothing
    if(name == "" || is.null(input$defineColorsMetadataMatrix)) return (NULL)
    if (is.null(global$defineColorsMetadataMatrix)) return (NULL)
    if(name %in% global$reservedVariables) return (NULL)
    
    # Get the values (with the factor ordering)
    values <- levels(factor(global$metadataMatrix[[name]]))
    values <- values[values != ""]
    
    # Get the colors
    colors <- hot_to_r(input$defineColorsMetadataMatrix)$Color
    
    global$defineColorsMetadataMatrix <- data.table(cbind(Value = values,  Color = colors))
  })
  
  # Open modal ====
  observeEvent(input$defineColorsMetadata, {
    # Check if there is metadata. If there is not, don't open the modal.
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    if (is.null(choices)) return (NULL)
    
    # Create and empty table and display it
    toggleModal(session, "defineColorsMetadataModal", toggle = "open")
  })
  
  # Update table ====
  observeEvent(c(input$defineColorsMetadataName, input$defineColorsMetadata, global$metadataColors), { # Do it also when clicking the button, so it cleans the table
    name <- input$defineColorsMetadataName
    global$defineColorsMetadataMatrix <- NULL
    
    # No metadata columns, do nothing
    if(name == "") return (NULL)
    
    # If the metadata column is reserved, do nothing
    if(name %in% global$reservedVariables) return (NULL)
    
    # If the entry is already present, usa that one, otherwise create a new one from scratch
    if (is.null(global$metadataColors[[name]]))
    {
      values <- levels(factor(global$metadataMatrix[[name]]))
      values <- values[values != ""]
      global$defineColorsMetadataMatrix <- data.table(Value = values, Color = rep("black", length(values)))
    }
    else
    {
      global$defineColorsMetadataMatrix <- global$metadataColors[[name]]
    }
    
    # If there are too many rows, put the table to NULL
    if (nrow(global$defineColorsMetadataMatrix) > global$tableLimit) global$defineColorsMetadataMatrix <- NULL
  }, ignoreInit = TRUE)
  
  # Handle the modification ====
  observeEvent(input$confirmDefineColorsMetadata, {
    if (is.null(global$defineColorsMetadataMatrix))
    {
      js$alertMessage("The colors must be defined first.")
      return (NULL)
    }
    
    # Get name
    name <- input$defineColorsMetadataName
    
    # Get values
    definedColors <- global$defineColorsMetadataMatrix
    definedColors$Color <- trimws(definedColors$Color)
    
    # If the colors are not valid, do not plot
    if (biomex.areColors(colors = definedColors$Color) == FALSE)
    {
      js$alertMessage("Invalid color detected, unable to save the color scheme.")
      return (NULL)
    }
    
    # Modify column
    global$metadataColors[[name]] <- definedColors
    global$defineColorsMetadataMatrix <- NULL
    
    # Close modal
    toggleModal(session, "defineColorsMetadataModal", toggle = "close")
  })
  
  # *** Export metadata ====
  # Handle the export ====
  observeEvent(input$exportMetadata, {
    if (is.null(global$metadataMatrix))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    if (input$metadataType == "Full") 
    {
      metadataMatrix <- global$metadataMatrix
    }
    else if (input$metadataType == "After pretreatment") 
    {
      if (is.null(global$dataMatrixNormalized))
      {
        js$alertMessage("No results available to export.")
        return (NULL)
      }
      
      metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = biomex.getObservationNames(dataMatrix = global$dataMatrixNormalized))
    }
    else if (input$metadataType == "Subset") 
    {
      resultList <- biomex.getDataInformationSubset(global = global, subsetType = input$metadataSubsetType)
      
      if (is.null(resultList$observationsSelected)) 
      {
        js$alertMessage("No results available to export.")
        return (NULL)
      }
      
      metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = resultList$observationsSelected)
    }
    
    global$exportTable <- metadataMatrix
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
}